# Inviting the public bot to your Discord server

Detailed instructions are available here : http://www.crouchingrancor.com/bot-documentation/

# Contact
If you have any question, problem or suggestion, please feel free to join this Discord Server : https://discord.gg/nyWgbU3

# Installing your own custom version of the bot
## Requirements

All you'll need is a computer connected to the Internet. Any OS that can run Java will do.

A bit of computer-science skills are required, since you'll need to compile the bot.

Of course, if you want the bot to be available 24/7, the computer that runs it will need to stay up 24/7.
You can also rent a small server, for example I was able to rent one for less than 4€/month.

Some examples of hosting services:
* https://aws.amazon.com/
* https://cloud.google.com/

## Installing the bot
/!\ This will most likely require a little bit of computer skills. If you don't feel like you'll make it, you should probably just invite the public version of the bot./!\

###Command-line installation (server)
1. You'll need to install a "Java runtime Environment (JRE)" with Java version 1.8. You can download a JRE from Oracle's website [here](http://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html).
1b. You'll also need Maven to compile the app
2. Clone this repository
3. Compile the app with Maven command `mvn package shade:shade`
4. Move the generated .jar so it is in the same directory as the settings.json file

###IDE Installation
1. Clone this repository
2. Run a Maven "project update"

###Configuration and launch
1. Follow [this quick tutorial](https://github.com/reactiflux/discord-irc/wiki/Creating-a-discord-bot-&-getting-a-token) to create a Discord bot and get a "token". This token is what helps the application connecting to your Discord server.
2. Rename settings\_template.json file as settings.json then edit it. In the first field, put the token you got from Discord at step 4 like in the following example : 	`"discordToken": "MzQyNTk4ODE5MjQzODg0NTU1.DGR9iA.rBI0QHIdCavRVi_fdZoFrh59vK4",`
3. Inside this same file, you should customize the "bot Admins" section. This defines who will be allowed to run administrator commands from Discord. You may use group names (as they are defined inside Discord) or individual user IDs (the 4-digits ID you can see in Discord).
4. You may customize anything else you like in the settings.json and messages_xx.json files, like the keywords used by the commands, the raid names, etc... But be careful not to change its structure, or the bot would fail to work correctly. 
5. Install mysql server [from here](https://dev.mysql.com/downloads/mysql/)
6. Use the config/database.sql file to create the required database
7. Configure mysql connection in the settings.json file
8. (command-line) Launch the bot using command `java -jar jarName.jar`
8b. (IDE) Launch the Main.java class
