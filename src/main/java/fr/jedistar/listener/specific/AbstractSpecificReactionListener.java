package fr.jedistar.listener.specific;

import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.javacord.api.entity.user.User;
import org.javacord.api.event.message.reaction.ReactionAddEvent;
import org.javacord.api.listener.message.reaction.ReactionAddListener;
import org.json.JSONObject;

import com.vdurmont.emoji.Emoji;

import fr.jedistar.StaticVars;

abstract class AbstractSpecificReactionListener implements ReactionAddListener {

	protected final Logger logger = LogManager.getLogger(this.getClass());

	final Emoji emojiOK;
	final Emoji emojiCancel;
	final User user;

	final JSONObject params;

	final String CANCEL_MESSAGE;
	final String SUCCESS_MESSAGE;
	final String DB_ERROR;
	
	private final String JSON_BASE = "specificListenerParams";
	private final String JSON_MESSAGES = "messages";
	private final String JSON_MESSAGES_CANCEL = "cancel";
	private final String JSON_MESSAGES_SUCCESS = "success";

	private final String JSON_ERRORS = "errors";
	private final String JSON_ERRORS_DB = "databaseError";
	
	AbstractSpecificReactionListener( User user,Emoji emojiOK, Emoji emojiCancel) {
		super();
		this.emojiOK = emojiOK;
		this.emojiCancel = emojiCancel;
		this.user = user;

		params = StaticVars.getJsonMessages().getJSONObject(JSON_BASE);
		
		JSONObject messages = params.getJSONObject(JSON_MESSAGES);
		CANCEL_MESSAGE = messages.getString(JSON_MESSAGES_CANCEL);
		SUCCESS_MESSAGE = messages.getString(JSON_MESSAGES_SUCCESS);
		
		JSONObject errors = params.getJSONObject(JSON_ERRORS);
		DB_ERROR = errors.getString(JSON_ERRORS_DB);
	}
	
	
	@Override
	public void onReactionAdd(ReactionAddEvent event) {

		if(event.getUser().isYourself()
				|| ! (event.getUser().getId() == user.getId()) ) {
			return;
		}
		
		if(emojiCancel.getUnicode().equals(event.getEmoji().asUnicodeEmoji().orElse(null))) {
			
			event.getChannel().sendMessage(CANCEL_MESSAGE);
			event.getMessage().ifPresent(message -> message.removeMessageAttachableListener(this));
			
			return;
		}

		if(!emojiOK.getUnicode().equals(event.getEmoji().asUnicodeEmoji().orElse(null))) {
			return;
		}
		
		Optional<String> userMessage = doReaction(event);
		
		userMessage.ifPresent(messageString -> event.getChannel().sendMessage(messageString));
		
		event.getMessage().ifPresent(message -> message.removeMessageAttachableListener(this));
	}

	/**
	 * Method implemented in each specific listener, that does the actual action
	 * @return A message that'll be sent to the channel
	 */
	protected abstract Optional<String> doReaction(ReactionAddEvent event);
}
