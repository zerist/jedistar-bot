package fr.jedistar.listener.specific;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Optional;

import org.javacord.api.entity.user.User;
import org.javacord.api.event.message.reaction.ReactionAddEvent;

import com.vdurmont.emoji.Emoji;

import fr.jedistar.StaticVars;

public class PayoutsUserDeleteReactionListener extends AbstractSpecificReactionListener {

	private String userName;
	private String channelID;

	private final static String SQL_DELETE_USER = "DELETE FROM payoutTime WHERE channelID=? AND userName=?";

	public PayoutsUserDeleteReactionListener(User user, Emoji emojiOK, Emoji emojiCancel, String userName,String channelID) {
		super(user, emojiOK, emojiCancel);
		this.userName = userName;
		this.channelID = channelID;
	}

	@Override
	protected Optional<String> doReaction(ReactionAddEvent event) {

		Connection conn = null;
		PreparedStatement stmt = null;

		try {
			conn = StaticVars.getJdbcConnection();

			stmt = conn.prepareStatement(SQL_DELETE_USER);

			stmt.setString(1, channelID);
			stmt.setString(2, userName);

			logger.info("executingQuery "+stmt.toString());
			stmt.executeUpdate();

			return Optional.of(SUCCESS_MESSAGE);
		}
		catch(SQLException e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			return Optional.of(DB_ERROR);
		}
		finally {
			try {
				if(stmt != null) {
					stmt.close();
				}
				if(conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
				logger.error(e.getMessage());
			}
		}


	}


}
