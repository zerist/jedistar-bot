package fr.jedistar.listener.specific;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

import org.javacord.api.entity.message.Message;
import org.javacord.api.entity.user.User;
import org.javacord.api.event.message.reaction.ReactionAddEvent;
import org.json.JSONArray;
import org.json.JSONObject;

import com.vdurmont.emoji.Emoji;

import fr.jedistar.StaticVars;
import help.swgoh.api.SwgohAPI;

public class PlayerAllyCodeInserter extends AbstractSpecificReactionListener {
	
	private final Integer allyCode;
	private final Message receivedMessage;
		
	private static final String INSERT_PLAYER = "REPLACE INTO player (discordUserID,allyCode) VALUES(?,?)";
	
	

	//Messages
	private final String MESSAGE_INSERTION_OK;
	private final String MESSAGE_LOADING;

	//Errors
	private final String ERROR_INSERTION_KO;
	private final String ERROR_ALLYCODE_NOT_FOUND;
	private final String ERROR_SWGOH_API;

	//JSON
	private static final String JSON_BASE = "playerAllyCodeInserterParams";
	private static final String JSON_MESSAGES = "messages";
	private static final String JSON_MESSAGES_INSERTION_OK = "insertionOK";

	private static final String JSON_ERRORS = "errors";
	private static final String JSON_ERRORS_DATABASE_ERROR = "databaseError";
	private static final String JSON_ALLYCODE_NOT_FOUND = "allyCodeNotFound";
	
	private static final String JSON_FIELD_NAME = "name";
	private static final String JSON_FIELD_GUILD = "guildName";
	
	private static final String JSON_SWGOH_API = "swgohApi";
	private static final String JSON_SWGOH_API_LOADING = "loadingFromSwgohApi";
	private static final String JSON_SWGOH_API_ERROR = "swgohApiError";

	
	public PlayerAllyCodeInserter(User user,Integer allyCode, Message receivedMessage,Emoji emojiOK, Emoji emojiCancel) {
		super(user,emojiOK, emojiCancel);
		
		this.allyCode = allyCode;
		this.receivedMessage = receivedMessage;
		
		
		JSONObject params = StaticVars.getJsonMessages();
		
		JSONObject swgohApiParams = params.getJSONObject(JSON_SWGOH_API);
		MESSAGE_LOADING = swgohApiParams.getString(JSON_SWGOH_API_LOADING);
		ERROR_SWGOH_API = swgohApiParams.getString(JSON_SWGOH_API_ERROR);

		JSONObject allyCodesParams = params.getJSONObject(JSON_BASE);
		
		
		JSONObject messages = allyCodesParams.getJSONObject(JSON_MESSAGES);
		MESSAGE_INSERTION_OK = messages.getString(JSON_MESSAGES_INSERTION_OK);

		JSONObject errors = allyCodesParams.getJSONObject(JSON_ERRORS);
		ERROR_INSERTION_KO = errors.getString(JSON_ERRORS_DATABASE_ERROR);
		ERROR_ALLYCODE_NOT_FOUND = errors.getString(JSON_ALLYCODE_NOT_FOUND);
	}

	@Override
	protected Optional<String> doReaction(ReactionAddEvent event) {

		insertAllyCode();

		return Optional.empty();
	}


	public void insertAllyCode() {
		
		CompletableFuture<Message> messageToDelete = receivedMessage.getChannel().sendMessage(MESSAGE_LOADING);
		
		SwgohAPI api = StaticVars.getSwgohApi();
		
		CompletableFuture<String> answerFuture = api.getPlayer(allyCode, SwgohAPI.PlayerField.name,SwgohAPI.PlayerField.guildName);
		
		answerFuture.exceptionally(e -> {
			logger.error(e.getMessage());
			e.printStackTrace();
			receivedMessage.getChannel().sendMessage(ERROR_SWGOH_API);
			return null;
		})
		.thenAccept(json -> {
			
			if(json == null) {
				return;
			}
			
			JSONArray answer = new JSONArray(json);
			
			messageToDelete.thenAccept(Message::delete);
			
			if(answer.length() == 0) {
				receivedMessage.getChannel().sendMessage(ERROR_ALLYCODE_NOT_FOUND);
			}
			else {
				try {
					insert();
					
					JSONObject player = answer.getJSONObject(0);
					
					String message = String.format(MESSAGE_INSERTION_OK, player.getString(JSON_FIELD_NAME),player.getString(JSON_FIELD_GUILD));
					receivedMessage.getChannel().sendMessage(message);
				}	
				catch(SQLException e) {
					logger.error(e.getMessage());
					e.printStackTrace();
					receivedMessage.getChannel().sendMessage(ERROR_INSERTION_KO);
				}
			}
		});
	}
	
	/**
	 * Insert the user data
	 * @return
	 * @throws SQLException
	 */
	public void insert() throws SQLException {
		Connection conn = null;
		PreparedStatement stmt = null;

		try {
			conn = StaticVars.getJdbcConnection();

			stmt = conn.prepareStatement(INSERT_PLAYER);

			stmt.setLong(1,receivedMessage.getAuthor().getId());
			stmt.setInt(2, allyCode);

			stmt.executeUpdate();
		}

		finally {

			try {
				if(stmt != null) {
					stmt.close();
				}
				if(conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
				logger.error(e.getMessage());
			}

		}
	}
}
