package fr.jedistar.listener.specific;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.javacord.api.entity.user.User;
import org.javacord.api.event.message.reaction.ReactionAddEvent;


import com.vdurmont.emoji.Emoji;

import fr.jedistar.StaticVars;

public class GuildNumberUpdateReactionListener extends AbstractSpecificReactionListener {

	private final Logger logger = LogManager.getLogger(this.getClass());

	private final String channelID;
	private final Integer guildID;
	
	private static final String UPDATE_GUILD_REQUEST = "UPDATE guild SET guildID=? WHERE channelID=?;";


	public GuildNumberUpdateReactionListener(User user, String channelID, Integer guildID, Emoji emojiOK, Emoji emojiCancel) {
		super(user,emojiOK, emojiCancel);

		this.channelID = channelID;
		this.guildID = guildID;
	}


	@Override
	protected Optional<String> doReaction(ReactionAddEvent event) {

		Connection conn = null;
		PreparedStatement stmt = null;

		try {
			conn = StaticVars.getJdbcConnection();

			stmt = conn.prepareStatement(UPDATE_GUILD_REQUEST);

			stmt.setInt(1, guildID);
			stmt.setString(2, channelID);

			logger.debug("Executing query : "+stmt.toString());
			stmt.executeUpdate();

			return Optional.of(SUCCESS_MESSAGE);
		}
		catch(SQLException e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			return Optional.of(DB_ERROR);
		}
		finally {
			try {
				if(stmt != null) {
					stmt.close();
				}
				if(conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				logger.error(e.getMessage());
			}	
		}
	}
	
}
