package fr.jedistar.listener.specific;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Time;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Optional;

import org.javacord.api.entity.user.User;
import org.javacord.api.event.message.reaction.ReactionAddEvent;

import com.vdurmont.emoji.Emoji;

import fr.jedistar.StaticVars;

public class PayoutsUserInsertionReactionListener extends AbstractSpecificReactionListener {

	private String userName;
	private GregorianCalendar payoutCalendar;
	private String flag;
	private String swgohggLink;
	private String channelID;

	private final static String SQL_INSERT_USER = "INSERT INTO payoutTime(channelID,userName,payoutTime,flag,swgohggLink) VALUES (?,?,?,?,?) ON DUPLICATE KEY UPDATE payoutTime=VALUES(payoutTime),flag=VALUES(flag),swgohggLink=VALUES(swgohggLink);";

	public PayoutsUserInsertionReactionListener(String userName,GregorianCalendar payoutCalendar, String flag, String swgohggLink, String channelID,User user, Emoji emojiOK, Emoji emojiCancel) {
		super(user, emojiOK, emojiCancel);
		this.userName = userName;
		this.payoutCalendar = payoutCalendar;
		this.flag = flag;
		this.swgohggLink = swgohggLink;
		this.channelID = channelID;
	}


	@Override
	protected Optional<String> doReaction(ReactionAddEvent event) {

		long timeMillisUTC = (long) payoutCalendar.get(Calendar.HOUR_OF_DAY) * 60 * 60 * 1000;
		timeMillisUTC = timeMillisUTC + (long) payoutCalendar.get(Calendar.MINUTE) * 60 * 1000;
		timeMillisUTC = timeMillisUTC - (long) payoutCalendar.getTimeZone().getOffset(System.currentTimeMillis());

		Connection conn = null;
		PreparedStatement stmt = null;

		try {
			conn = StaticVars.getJdbcConnection();

			stmt = conn.prepareStatement(SQL_INSERT_USER);

			stmt.setString(1,channelID);
			stmt.setString(2,userName);
			stmt.setTime(3, new Time(timeMillisUTC));
			stmt.setString(4, ":"+flag+":");
			stmt.setString(5, swgohggLink);

			logger.info("executingQuery "+stmt.toString());
			stmt.executeUpdate();

			return Optional.of(SUCCESS_MESSAGE);
		}
		catch(SQLException e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			return Optional.of(DB_ERROR);
		}
		finally {
			try {
				if(stmt != null) {
					stmt.close();
				}
				if(conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
				logger.error(e.getMessage());
			}
		}


	}

}
