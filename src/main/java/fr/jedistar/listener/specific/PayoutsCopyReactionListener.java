/**
 * 
 */
package fr.jedistar.listener.specific;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Optional;

import org.javacord.api.entity.user.User;
import org.javacord.api.event.message.reaction.ReactionAddEvent;

import com.vdurmont.emoji.Emoji;

import fr.jedistar.StaticVars;


public class PayoutsCopyReactionListener extends AbstractSpecificReactionListener {


	private String channelID;
	private String sourceChannelID;
	
	private final static String SQL_COPY_CHAN = "CALL copyPayouts(?,?);";
	
	public PayoutsCopyReactionListener(User user, Emoji emojiOK, Emoji emojiCancel, String channelID,String sourceChannelID) {
		super(user, emojiOK, emojiCancel);
		this.channelID = channelID;
		this.sourceChannelID = sourceChannelID;
	}


	@Override
	protected Optional<String> doReaction(ReactionAddEvent event) {
		Connection conn = null;
		PreparedStatement stmt = null;

		try {
			conn = StaticVars.getJdbcConnection();
			
			stmt = conn.prepareStatement(SQL_COPY_CHAN);
			
			stmt.setString(1, sourceChannelID);
			stmt.setString(2, channelID);
			
			logger.info("executingQuery "+stmt.toString());
			stmt.executeUpdate();
			
			return Optional.of(SUCCESS_MESSAGE);
		}
		catch(SQLException e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			return Optional.of(DB_ERROR);
		}
		finally {
			try {
				if(stmt != null) {
					stmt.close();
				}
				if(conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
				logger.error(e.getMessage());
			}
		}
	}

}
