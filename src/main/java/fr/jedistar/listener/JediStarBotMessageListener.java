package fr.jedistar.listener;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import fr.jedistar.commands.*;
import fr.jedistar.exception.AllyCodeNotFoundException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.javacord.api.entity.Region;
import org.javacord.api.entity.message.Message;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import org.javacord.api.entity.permission.Role;
import org.javacord.api.entity.server.Server;
import org.javacord.api.entity.user.User;
import org.javacord.api.event.message.MessageCreateEvent;
import org.javacord.api.listener.message.MessageCreateListener;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.vdurmont.emoji.Emoji;

import fr.jedistar.JediStarBotCommand;
import fr.jedistar.StaticVars;
import fr.jedistar.formats.CommandAnswer;
import fr.jedistar.formats.PendingAction;

public class JediStarBotMessageListener implements MessageCreateListener {

	private final Logger logger = LogManager.getLogger(this.getClass());

	public static final String PREFIXE_COMMANDES = "%";

	private final Map<String,JediStarBotCommand> commandsMap;

	private final String MESSAGE;

	private final static String SQL_BANNED_IDS = "SELECT * FROM bannedIds WHERE discordId=?";
	//error messages
	private final String ERROR_ALLYCODE_NOTFOUND;
	private final String ERROR_ALLYCODE_NOTFOUND_FOR_USER;
	private final String ERROR_LOCAL_DATABASE;

	//Noms des champs JSON
	private static final String JSON_ADMINS = "botAdmins";
	private static final String JSON_GROUPS = "groups";
	private static final String JSON_USERS = "users";
	private static final String JSON_BASE_MESSAGE = "baseMessage";

	private static final String JSON_COMMON_ERRORS = "commonErrors";
	private static final String JSON_COMMON_ERRORS_ALLYCODE = "AllyCodeNotFound";
	private static final String JSON_COMMON_ERRORS_ALLYCODE_FOR_USER = "AllyCodeForUserNotFound";
	private static final String JSON_COMMON_ERRORS_LOCAL_DB = "localDbError";


	private Set<String> adminGroups;
	private Set<Integer> adminUsers;

	private static final String SQL_INSERT_HISTORY = "INSERT INTO commandHistory(command,ts,userID,userName,serverID,serverName,serverRegion,executionTime) VALUES (?,?,?,?,?,?,?,?);";

	public JediStarBotMessageListener() {
		super();


		commandsMap = new HashMap<String,JediStarBotCommand>();

		//AJOUTER ICI DE NOUVELLES COMMANDES
		
		try {
			RaidCommand raid = new RaidCommand();
			ModsCommand mods = new ModsCommand();
			AreneCommand arene = new AreneCommand();
			SetUpCommand setup = new SetUpCommand();
			TerritoryBattlesCommand tb = new TerritoryBattlesCommand();
			HelpCommand help = new HelpCommand();
			PayoutCommand payout = new PayoutCommand();
			GuildReportCommand guildReport = new GuildReportCommand();
			ZetasCommand zetas = new ZetasCommand();
			MyModsCommand mymods = new MyModsCommand();
			RefreshLiveDataCommand liveData = new RefreshLiveDataCommand();
			
			commandsMap.put(raid.getCommand(), raid);
			commandsMap.put(mods.getCommand(), mods);
			commandsMap.put(arene.getCommand(), arene);
			commandsMap.put(setup.getCommand(),setup);
			commandsMap.put(tb.getCommand(), tb);
			commandsMap.put(help.getCommand(), help);
			commandsMap.put(payout.getCommand(), payout);
			commandsMap.put(guildReport.getCommand(), guildReport);
			commandsMap.put(zetas.getCommand(),zetas);
			commandsMap.put(mymods.getCommand(),mymods);
			commandsMap.put(liveData.getCommand(), liveData);
			
		}
		catch(JSONException e) {
			logger.error("JSON FILE PROBLEM : " + e.getMessage());
			e.printStackTrace();
		}


		

		//Lecture du Json

		JSONObject parameters = StaticVars.getJsonSettings();

		//message de base
		MESSAGE = StaticVars.getJsonMessages().getString(JSON_BASE_MESSAGE);

		//Erreurs communes
		JSONObject commonErrors = StaticVars.getJsonMessages().getJSONObject(JSON_COMMON_ERRORS);
		ERROR_ALLYCODE_NOTFOUND = commonErrors.getString(JSON_COMMON_ERRORS_ALLYCODE);
		ERROR_ALLYCODE_NOTFOUND_FOR_USER = commonErrors.getString(JSON_COMMON_ERRORS_ALLYCODE_FOR_USER);
		ERROR_LOCAL_DATABASE = commonErrors.getString(JSON_COMMON_ERRORS_LOCAL_DB);

		//admins
		JSONObject jsonAdmins = parameters.getJSONObject(JSON_ADMINS);
		JSONArray jsonAdminGroups = jsonAdmins.getJSONArray(JSON_GROUPS);
		adminGroups = new HashSet<String>();

		for(int i=0 ; i<jsonAdminGroups.length() ; i++) {
			adminGroups.add(jsonAdminGroups.getString(i));
		}

		JSONArray jsonAdminUsers = jsonAdmins.getJSONArray(JSON_USERS);
		adminUsers = new HashSet<Integer>();

		for(int i=0 ; i<jsonAdminUsers.length() ; i++) {
			adminUsers.add(jsonAdminUsers.getInt(i));
		}

	}
	

	public void onMessageCreate(MessageCreateEvent event) {

		final Message receivedMessage = event.getMessage();

		//Check database for banned users
		Long discordUserId = event.getMessageAuthor().getId();
		try(
			Connection conn = StaticVars.getJdbcConnection();
			PreparedStatement stmt = conn.prepareStatement(SQL_BANNED_IDS);
		){
			
			stmt.setLong(1, discordUserId);
			
			ResultSet rs = stmt.executeQuery();
			
			if(rs.next()) {
				//User is banned !!
				return;
			}
		} catch (SQLException e) {
			logger.error("database error when getting banned ids",e);
			e.printStackTrace();
		}
		
		String messageAsString = receivedMessage.getContent().toLowerCase();

		//Si le message est vide ou ne commence pas par % : Ne rien faire.
		if(messageAsString == null
				|| !messageAsString.startsWith(PREFIXE_COMMANDES)) {
			return;
		}
		
		//On retire le préfixe
		messageAsString = messageAsString.substring(1);

		//On éclate les différentes parties du message
		String[] messagePartsArray = messageAsString.split(" ");

		ArrayList<String> messageParts = new ArrayList<String>();
		for(String param : messagePartsArray) {
			if(param != null && !"".equals(param)) {
				messageParts.add(param.trim());
			}
		}

		messageParts.remove(0);

		String command = messagePartsArray[0];

		JediStarBotCommand botCommand = commandsMap.get(command);

		if(botCommand == null) {
			return;
		}
		Long startTimestamp = System.currentTimeMillis();
		
		if(receivedMessage.getChannel() != null) {
			receivedMessage.getChannel().type();
		}

		boolean isAdmin = isAdmin(receivedMessage);

		//generate answer
		String mentionTag = receivedMessage.getUserAuthor().map(User::getMentionTag).orElse("");
		try {
			final CommandAnswer answer = botCommand.answer(messageParts, receivedMessage, isAdmin);

			if(answer == null) {
				return;
			}

			String message ="";
			if(!"".equals(answer.getMessage())) {
				message = String.format(MESSAGE, mentionTag,answer.getMessage());
			}

			EmbedBuilder embed = answer.getEmbed();

			if(embed != null) {
				embed.addField("-","Bot designed by [the french community Dark Side Of The Moon (DSOTM)](https://jedistar.jimdo.com)", false);
			}

			CompletableFuture<Message> future = receivedMessage.getChannel().sendMessage(message, embed);

			//New Java 8 syntax
			future.thenAccept(sentMessage -> {

				for(Emoji reaction : answer.getReactions()) {
					sentMessage.addReaction(reaction.getUnicode());
				}

				for(PendingAction action : answer.getPendingActions()) {
					action.setMessage(sentMessage);
				}

				sentMessage.addMessageAttachableListener(answer.getReactionAddListener()).forEach(manager-> manager.removeAfter(10, TimeUnit.MINUTES));

			});

		}
		catch (SQLException e){
			e.printStackTrace();
			logger.error(e.getMessage());
			receivedMessage.getChannel().sendMessage(String.format(MESSAGE,mentionTag,ERROR_LOCAL_DATABASE));
		} catch (AllyCodeNotFoundException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			
			
			if(e.getDiscordID() != null) {
				receivedMessage.getApi().getCachedUserById(e.getDiscordID()).ifPresent(user -> {
					String param = String.format(ERROR_ALLYCODE_NOTFOUND_FOR_USER,user.getMentionTag());
					
					receivedMessage.getChannel().sendMessage(String.format(MESSAGE,mentionTag,String.format(ERROR_ALLYCODE_NOTFOUND,param)));

				});
			}
			else {				
				receivedMessage.getChannel().sendMessage(String.format(MESSAGE,mentionTag,String.format(ERROR_ALLYCODE_NOTFOUND,e.getAllyCode())));
			}
		}

		insertCommandHistory(command, receivedMessage, startTimestamp);

	}

	private boolean isAdmin(Message messageRecu) {

		Optional<User> author = messageRecu.getUserAuthor();
		Optional<Server> server = messageRecu.getServer();

		//Webhook case
		if(!author.isPresent()) {
			return false;
		}

		//PM case
		if(!server.isPresent()) {
			return true;
		}

		//Nominal case

		if(adminUsers.contains(Integer.parseInt(author.get().getDiscriminator()))){
			return true;
		}


		for(Role role : author.get().getRoles(server.get())) {
			if(adminGroups.contains(role.getName())) {
				return true;
			}
		}

		return false;
	}

	private void insertCommandHistory(String command,Message receivedMessage,Long startTimestamp) {
		Connection conn = null;
		PreparedStatement stmt = null;

		Long duration = System.currentTimeMillis() - startTimestamp;
		
		try {
			conn = StaticVars.getJdbcConnection();

			stmt = conn.prepareStatement(SQL_INSERT_HISTORY);

			stmt.setString(1,command);

			java.sql.Timestamp ts = new Timestamp(new Date().getTime());
			stmt.setTimestamp(2, ts);

			stmt.setString(3, Long.toString(receivedMessage.getAuthor().getId()));
			stmt.setString(4, receivedMessage.getAuthor().getName().replaceAll("\\p{C}", "?"));
			
			Optional<Server> server = receivedMessage.getServer();

			if(server.isPresent()) {
				stmt.setString(5, Long.toString(server.map(Server::getId).orElse(0L)));
			}
			else {
				stmt.setNull(5,java.sql.Types.VARCHAR);
			}

			stmt.setString(6, server.map(Server::getName).orElse(null));
			stmt.setString(7, server.map(Server::getRegion).orElse(Region.UNKNOWN).getName());

			stmt.setLong(8, duration);

			stmt.executeUpdate();
		}
		catch(SQLException e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		finally {
			if(stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					logger.error(e.getMessage());
				}
			}
			if(conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					logger.error(e.getMessage());
				}
			}
		}
	}
}
