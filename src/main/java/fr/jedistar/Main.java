package fr.jedistar;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Timer;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;


import fr.jedistar.commands.ModsCommand;
import fr.jedistar.livedata.LiveDataRefresher;
import help.swgoh.api.SwgohAPI;
import help.swgoh.api.SwgohAPI.Language;

public class Main {

	final static Logger logger = LogManager.getLogger(Main.class);

	// Noms des éléments dans le fichier de paramètres
	private static final String PARAM_MODS_JSON_URI = "modsJsonURI";
	private static final String PARAM_TOKEN = "discordToken";
	private static final String PARAM_DB = "database";
	private static final String PARAM_DB_URL = "url";
	private static final String PARAM_DB_USER = "user";
	private static final String PARAM_DB_PWD = "pwd";
	private static final String PARAM_MESSAGES = "messages";
	private static final String PARAM_SWGOH_API = "swgohAPI";
	private static final String PARAM_CHAR_PORTRAITS= "characterPortraits";
	private static final String PARAM_CHAR_PORTRAITS_LOCAL= "localDir";
	private static final String PARAM_CHAR_PORTRAITS_URI= "externalURI";

	private static final String DEFAULT_PARAMETERS_FILE = "settings.json";
	private static final String DEFAULT_MESSAGE_FILE_NAME = "messages_";
	private static final String DEFAULT_MESSAGE_FILE_EXT = ".json";

	private static String url;
	private static String user;
	private static String passwd;
	
	private static String swgohApiUser;
	private static String swgohApiPass;

	public static void main(String... args) {

		String parametersFilePath = "";
		String messagesFilePath =DEFAULT_MESSAGE_FILE_NAME ;

		// Si un argument, on l'utilise comme chemin au fichier de paramètres
		if (args.length != 0) {
			parametersFilePath = args[0];
		}
		// Sinon, on utilise le chemin par défaut
		else {
			parametersFilePath = DEFAULT_PARAMETERS_FILE;
		}

		if(args.length > 1 && "noCache".equals(args[1])) {
			StaticVars.useCache = false;
		}

		String token = "";
		String langParam = null;
		// Lecture du fichier Json et récupération des paramètres
		try {
			// Lecture du fichier
			byte[] encoded = Files.readAllBytes(Paths.get(parametersFilePath));
			String parametersJson = new String(encoded, "utf-8");

			// D�codage du json
			JSONObject parameters = new JSONObject(parametersJson);

			StaticVars.jsonSettings = parameters;

			// METTRE LA LECTURE DES PARAMETRES DU PLUS IMPORTANT AU MOINS IMPORTANT
			// Lecture du token Discord
			token = parameters.getString(PARAM_TOKEN);

			// URI et encodage du JSON des mods conseillés
			String modsJsonUri = parameters.getString(PARAM_MODS_JSON_URI);
			ModsCommand.setJsonUri(modsJsonUri);

			JSONObject dbParams = parameters.getJSONObject(PARAM_DB);
			url = dbParams.getString(PARAM_DB_URL);
			user = dbParams.getString(PARAM_DB_USER);
			passwd = dbParams.getString(PARAM_DB_PWD);

			JSONObject swgohApiParams = parameters.getJSONObject(PARAM_SWGOH_API);
			swgohApiUser = swgohApiParams.getString(PARAM_DB_USER);
			swgohApiPass = swgohApiParams.getString(PARAM_DB_PWD);
			
			langParam = parameters.getString(PARAM_MESSAGES);
			messagesFilePath+=langParam+DEFAULT_MESSAGE_FILE_EXT;

			JSONObject charPortraitParams = parameters.getJSONObject(PARAM_CHAR_PORTRAITS);
			StaticVars.charPortraitLocalDir = charPortraitParams.getString(PARAM_CHAR_PORTRAITS_LOCAL);
			StaticVars.charPortraitURI = charPortraitParams.getString(PARAM_CHAR_PORTRAITS_URI);

		} catch (IOException e) {
			logger.error("Cannot read the parameters file " + parametersFilePath);
			e.printStackTrace();
			return;
		} catch (JSONException e) {
			logger.error("JSON parameters file is incorrectly formatted");
			e.printStackTrace();
			return;
		}

		try {
			// Lecture du fichier

			byte[] encoded = Files.readAllBytes(Paths.get(messagesFilePath));
			String messagesJson = new String(encoded, "utf-8");

			// Décodage du json
			JSONObject messages = new JSONObject(messagesJson);

			StaticVars.jsonMessages = messages;

		} catch (IOException e) {
			logger.error("Cannot read the parameters file " + parametersFilePath);
			e.printStackTrace();
			return;
		} catch (JSONException e) {
			logger.error("JSON parameters file is incorrectly formatted");
			e.printStackTrace();
			return;
		}

		// Initialisation bdd
		try {
			//Local Database
			StaticVars.setJdbcParams(url, user, passwd);
			logger.info("testing local database connection");
			Connection conn = StaticVars.getJdbcConnection();
			logger.info("database connection OK");


			conn.close();
		} catch (SQLException e) {
			logger.error("Error connecting to local mysql database");
			e.printStackTrace();
			return;
		}

		logger.info("initiating swgoh api");
		StaticVars.initSwgohApi(swgohApiUser, swgohApiPass);
		logger.info("swgoh api OK");
		
		logger.info("launching scheduled data refresher");
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 5);
		cal.set(Calendar.MINUTE,0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND,0);
		
		cal.add(Calendar.HOUR_OF_DAY, 24);
		
		Language selectedLanguage = null;
		
		for(Language lang : SwgohAPI.Language.values()) {
			if(StringUtils.containsIgnoreCase(lang.name(), langParam)) {
				selectedLanguage = lang;
				break;
			}
		}
		
		if(selectedLanguage == null) {
			logger.error("Error finding language");
		}
		else {
			new Timer().schedule(new LiveDataRefresher(selectedLanguage),cal.getTime(),TimeUnit.HOURS.toMillis(24));
			StaticVars.lang = selectedLanguage;
		}
		logger.info("Launching bot with token -" + token + "-");

		JediStarBot bot = new JediStarBot(token);

		bot.connect();

	}
}
