package fr.jedistar.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.javacord.api.entity.user.User;

import fr.jedistar.StaticVars;
import fr.jedistar.exception.AllyCodeNotFoundException;

public class DbUtils {

	private final Logger logger = LogManager.getLogger(this.getClass());

	private final static String SQL_FIND_ALLYCODE = "SELECT allyCode FROM player WHERE discordUserID=?";


	public static Integer findAllyCode(User user) throws SQLException, AllyCodeNotFoundException {

		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		try{
			conn = StaticVars.getJdbcConnection();

			stmt = conn.prepareStatement(SQL_FIND_ALLYCODE);

			stmt.setLong(1,user.getId());

			rs = stmt.executeQuery();

			if(!rs.next()){
				throw new AllyCodeNotFoundException(user.getId());
			}

			return rs.getInt("allyCode");
		}
		finally {
			if(rs != null)
				rs.close();
			if(stmt != null)
				stmt.close();
			if(conn != null)
				conn.close();
		}
	}
}
