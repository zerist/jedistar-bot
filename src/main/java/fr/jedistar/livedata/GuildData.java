package fr.jedistar.livedata;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import fr.jedistar.StaticVars;
import fr.jedistar.formats.GuildAllyCodesList;
import help.swgoh.api.SwgohAPI;
import help.swgoh.api.SwgohAPIFilter;

public abstract class GuildData {

	protected static final Logger logger = LogManager.getLogger(GuildData.class);
	
	private final static String SQL_SELECT_GUILD_UNITS_EXPIRATION = "SELECT MIN(expiration) as expiration FROM guildUnits WHERE guildID=?";
	private final static String SQL_INSERT_GUILD_UNITS = "REPLACE INTO guildUnits (guildID,player,charID,rarity,combatType,power,level,gearLevel,zetas,expiration) VALUES (?,?,?,?,?,?,?,?,?,?) ;";
	private final static String SQL_DELETE_EXPIRED_GUILD_UNITS = "DELETE FROM guildUnits WHERE guildID=? AND expiration < ?";
	
	private final static String SQL_FIND_GUILD_ID_FROM_ALLY_CODE = "SELECT guildID,guildIDExpiration FROM player WHERE allyCode = ?";
	
	private final static String SQL_INSERT_GUILD_ID = "UPDATE player SET guildID=?,guildIDExpiration=? WHERE allyCode=?";

	/**
	 * Refreshes the guild data from swgoh.help
	 * @param allyCode
	 * @param guildID
	 * @return the last full refresh date
	 */
	public static CompletableFuture<Calendar> refreshGuildData(Integer allyCode,String guildID) {

		if(refreshNeeded(guildID)) {
			SwgohAPI api = StaticVars.getSwgohApi();

			SwgohAPIFilter filter = new SwgohAPIFilter("id")
					.and("roster",new SwgohAPIFilter("allyCode")
							);

			CompletableFuture<String> future = api.getLargeGuild(allyCode,filter);
			
			CompletableFuture<GuildAllyCodesList> allyCodesListFuture = future.thenApply(json -> {
				try {
					insertGuildID(allyCode, guildID);
				} catch(SQLException e) {
					//Should not happen, not blocking for execution
					e.printStackTrace();
					logger.error(e.getMessage());
				}
				
				return buildAllyCodesList(json);
			});		
			
			return allyCodesListFuture.thenApply(list -> {
				
				SwgohAPIFilter filter2 = new SwgohAPIFilter("name","updated","allyCode")
						.and("roster", new SwgohAPIFilter("defId","rarity","combatType","gp","level","gear")
								.and("skills",new SwgohAPIFilter("tier","isZeta")
										)
								);
				
				CompletableFuture<String> rostersFuture = api.getPlayers(list, filter2);
						
				try {
					//Have to run this synchronously to avoid getting nested futures (would be CompletablFuture<CompletableFuture<Calendar>>)
					//But that's no problem because we already are inside a side Thread
					return rostersFuture.thenApply(json -> {
						return insert(json,list.getGuildId());
					}).get();
					
				} catch (InterruptedException | ExecutionException e) {
					e.printStackTrace();
					logger.error(e.getMessage());
					throw new RuntimeException(e);
				}
			});
			
		}
		else {
			return CompletableFuture.completedFuture(null);
		}
	}
	
	private static GuildAllyCodesList buildAllyCodesList(String guildJson){

		JSONObject guildData = new JSONArray(guildJson).getJSONObject(0);

		String guildID = guildData.getString("id");
		
		GuildAllyCodesList list = new GuildAllyCodesList(guildID);

		JSONArray guildRoster = guildData.getJSONArray("roster");
		
		for(int i = 0 ; i < guildRoster.length() ; i++) {
			Integer allyCode = guildRoster.getJSONObject(i).getInt("allyCode");
			list.add(allyCode);
		}
		
		return list;
	}
	
	private static Calendar insert(String json,String guildID) throws JSONException {

		try {
			JSONArray rostersList = new JSONArray(json);


			Calendar minExpirationCal = Calendar.getInstance();
			minExpirationCal.add(Calendar.DAY_OF_MONTH, 1);

			Connection conn = null;

			PreparedStatement stmt = null;

			try {
				conn = StaticVars.getJdbcConnection();
				stmt = conn.prepareStatement(SQL_INSERT_GUILD_UNITS);

				for(int i = 0 ; i < rostersList.length() ; i++) {


					JSONObject playerData = rostersList.getJSONObject(i);

					String playerName = playerData.getString("name");

					Long expirationTimestamp = playerData.getLong("updated");
					Calendar expirationCal = Calendar.getInstance();
					expirationCal.setTime(new Date(expirationTimestamp));
					expirationCal.add(Calendar.DAY_OF_MONTH, 1);
					java.sql.Timestamp expiration = new Timestamp(expirationCal.getTimeInMillis());

					if(minExpirationCal.after(expirationCal)) {
						minExpirationCal = expirationCal;
					}

					JSONArray charArray = playerData.getJSONArray("roster");

					for(int j=0 ; j < charArray.length() ; j++) {
						try {
							JSONObject charData = charArray.getJSONObject(j);

							stmt.setString(1,guildID);
							stmt.setString(2,playerName);
							stmt.setString(3,charData.getString("defId"));
							stmt.setInt(4, charData.getInt("rarity"));
							stmt.setInt(5,charData.getInt("combatType"));
							stmt.setInt(6,charData.getInt("gp"));
							stmt.setInt(7, charData.getInt("level"));
							stmt.setInt(8, charData.getInt("gear"));

							//Zetas
							Integer zetasNumber = 0;

							JSONArray skills = charData.getJSONArray("skills");

							for(int k = 0 ; k < skills.length() ; k++) {
								if(skills.getJSONObject(k).getBoolean("isZeta") && 8 == skills.getJSONObject(k).getInt("tier")) {
									zetasNumber ++;
								}
							}

							stmt.setInt(9, zetasNumber);
							stmt.setTimestamp(10, expiration);

							stmt.addBatch();
						}
						catch(JSONException e) {
							logger.info("Incorrectly formatted guild unit\r\n"+charArray.get(i).toString());
							continue;
						}
					}
				}

				stmt.executeBatch();

				//clear obsolete data
				stmt.close();

				stmt = conn.prepareStatement(SQL_DELETE_EXPIRED_GUILD_UNITS);

				stmt.setString(1, guildID);

				java.sql.Timestamp expiration = new Timestamp(minExpirationCal.getTimeInMillis());
				stmt.setTimestamp(2, expiration);

				stmt.executeUpdate();

				//Return last full update date
				minExpirationCal.add(Calendar.DAY_OF_MONTH, -1);		
				return minExpirationCal;

			}
			finally {
				if(stmt != null) {
					stmt.close();
				}
				if(conn != null) {
					conn.close();
				}
			}
		}
		catch(SQLException e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	
	public static boolean refreshNeeded(String guildID) {
		
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			conn = StaticVars.getJdbcConnection();

			stmt = conn.prepareStatement(SQL_SELECT_GUILD_UNITS_EXPIRATION);

			stmt.setString(1, guildID);
			
			rs = stmt.executeQuery();

			boolean updateNeeded = true;

			if(rs.next()) {
				Date expiration = rs.getTimestamp("expiration");
				
				if(expiration != null && expiration.after(new Date())) {
					updateNeeded = false;
				}
			}
			
			return updateNeeded;
		}
		catch(SQLException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			return true;
		}
		finally {
			try {
				if(rs != null) {
					rs.close();
				}
				if(stmt != null) {
					stmt.close();
				}
				if(conn != null) {
					conn.close();
				}
			}
			catch(SQLException e) {
				//Should never happen
				e.printStackTrace();
				logger.error(e.getMessage());
			}
		}
	}
	
	public static CompletableFuture<String> getGuildIDFromAllyCode(Integer allyCode) throws SQLException {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			conn = StaticVars.getJdbcConnection();

			stmt = conn.prepareStatement(SQL_FIND_GUILD_ID_FROM_ALLY_CODE);
			
			stmt.setInt(1,allyCode);
			
			rs = stmt.executeQuery();
			
			if(!rs.next()) {
				return refreshGuildIDFromAllyCode(allyCode);
			}
			
			Timestamp expiration = rs.getTimestamp("guildIDExpiration");
			if(expiration.before(new Date())) {
				return refreshGuildIDFromAllyCode(allyCode);
			}
			else {
				return CompletableFuture.completedFuture(rs.getString("guildID"));
			}
		}
		finally {
			try {
				if(rs != null) {
					rs.close();
				}
				if(stmt != null) {
					stmt.close();
				}
				if(conn != null) {
					conn.close();
				}
			}
			catch(SQLException e) {
				//Should never happen
				e.printStackTrace();
				logger.error(e.getMessage());
			}
		}
	}
	
	private static CompletableFuture<String> refreshGuildIDFromAllyCode(Integer allyCode) {
		
		CompletableFuture<String> future = StaticVars.getSwgohApi().getGuild(allyCode, new SwgohAPIFilter("id"));
		
		return future.thenApply(json ->{
			JSONObject guildData = new JSONArray(json).getJSONObject(0);
			
			String guildID = guildData.getString("id");
			
			try {
				insertGuildID(allyCode,guildID);
			}
			catch(SQLException e) {
				//Should not happen, not blocking for execution
				e.printStackTrace();
				logger.error(e.getMessage());
			}
			
			return guildID;
		});
	}
	
	private static void insertGuildID(Integer allyCode,String guildID) throws SQLException{
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = StaticVars.getJdbcConnection();

			stmt = conn.prepareStatement(SQL_INSERT_GUILD_ID);
			
			
			Calendar expirationCal = Calendar.getInstance();
			expirationCal.add(Calendar.DAY_OF_MONTH, 1);
			java.sql.Timestamp expiration = new Timestamp(expirationCal.getTimeInMillis());
			
			stmt.setString(1,guildID);
			stmt.setTimestamp(2, expiration);
			stmt.setInt(3, allyCode);
			
			stmt.executeUpdate();
		
		}
		finally {
			if(stmt != null) {
				stmt.close();
			}
			if(conn != null) {
				conn.close();
			}
		}
	}
	
}
