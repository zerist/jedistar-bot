package fr.jedistar.livedata;

public class Character{
	public String name;
	public String baseID;
	public String image;
	
	public final static Integer CHARA_TYPE_TOON = 1;
	public final static Integer CHARA_TYPE_SHIP = 2;

	public Character(String name,String baseID,String image) {
		this.name= name;
		this.baseID = baseID;
		this.image= image;
	}

}
