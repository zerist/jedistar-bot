package fr.jedistar.livedata;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.jedistar.StaticVars;
import help.swgoh.api.SwgohAPI.Language;

public abstract class CharacterData {

	protected final Logger logger = LogManager.getLogger(this.getClass());

	private final static String SQL_GET_CHARS_BY_NAME = "SELECT * FROM liveDataCharacters WHERE name LIKE ? AND lang=?;";
	private final static String SQL_GET_CHARS_BY_ID = "SELECT * FROM liveDataCharacters WHERE baseId = ? AND lang=?;";

	public static List<Character> findMatchingCharacters(String name, Language lang) throws SQLException{

		List<Character> result = new ArrayList<Character>();

		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {
			conn = StaticVars.getJdbcConnection();

			stmt = conn.prepareStatement(SQL_GET_CHARS_BY_NAME);

			String matcher = "%"+StringUtils.replaceChars(name, ' ', '%')+"%";

			stmt.setString(1, matcher);
			stmt.setString(2, lang.getSwgohCode());

			rs = stmt.executeQuery();

			while(rs.next()) {
				result.add(new Character(rs.getString("name"),rs.getString("baseId"),String.format(StaticVars.getCharPortraitURI(), rs.getString("baseId"))));
			}

			return result;
		}
		finally {
			if(rs != null) {
				rs.close();
			}
			if(stmt != null) {
				stmt.close();
			}
			if(conn != null) {
				conn.close();
			}
		}
	}

	public static Character findCharacterById(String id, Language lang) throws SQLException{

		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {
			conn = StaticVars.getJdbcConnection();

			stmt = conn.prepareStatement(SQL_GET_CHARS_BY_ID);

			stmt.setString(1, id);
			stmt.setString(2, lang.getSwgohCode());

			rs = stmt.executeQuery();

			if(rs.next()) {
				return new Character(rs.getString("name"),rs.getString("baseId"),String.format(StaticVars.getCharPortraitURI(), rs.getString("baseId")));
			}

			return null;
		}
		finally {
			if(rs != null) {
				rs.close();
			}
			if(stmt != null) {
				stmt.close();
			}
			if(conn != null) {
				conn.close();
			}
		}
	}
}
