package fr.jedistar.livedata;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimerTask;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import javax.imageio.ImageIO;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import fr.jedistar.StaticVars;
import help.swgoh.api.SwgohAPI;
import help.swgoh.api.SwgohAPI.Language;
import help.swgoh.api.SwgohAPIFilter;
import help.swgoh.api.image.ToonImageRequestBuilder;

public class LiveDataRefresher extends TimerTask {

	private final Logger logger = LogManager.getLogger(this.getClass());
	
	private final Language lang;
	
	private final static String JSON_API_ID = "id";
	private final static String JSON_API_BASE_ID = "baseId";
	private final static String JSON_API_NAME_KEY = "nameKey";
	private final static String JSON_API_ABILITY_REFERENCE = "abilityReference";
	private final static String JSON_API_OBTAINABLE = "obtainable";
	private final static String JSON_API_RARITY = "rarity";
	
	private final static String SQL_INSERT_CHAR = "REPLACE INTO liveDataCharacters (baseId,name,lang) VALUES (?,?,?);";
	private final static String SQL_INSERT_SKILLS = "REPLACE INTO liveDataSkills (id,name,lang) VALUES (?,?,?);";


	public LiveDataRefresher(Language lang) {
		super();
		
		this.lang = lang;
	}
			
	@Override
	public void run() {	
				
		List<String> baseIdList = new ArrayList<String>();
		
		Connection conn = null;
		PreparedStatement stmt = null;
		
		try {
			SwgohAPI api = StaticVars.getSwgohApi();
		
			Map<String,Object> searchCriteria = new HashMap<String,Object>();
			searchCriteria.put(JSON_API_OBTAINABLE,true);
			searchCriteria.put(JSON_API_RARITY, 7);
			
			CompletableFuture<String> future = api.getSupportData(SwgohAPI.Collection.unitsList,searchCriteria, lang,new SwgohAPIFilter(JSON_API_BASE_ID,JSON_API_NAME_KEY));
			
			//We want this to be a synchronous method so we'll just wait on the completableFuture to complete...
			String charString = future.get();
			
			JSONArray charArray = new JSONArray(charString);
			
			conn = StaticVars.getJdbcConnection();
			
			conn.setAutoCommit(false);
			
			stmt = conn.prepareStatement(SQL_INSERT_CHAR);

			for(int i = 0 ; i < charArray.length() ; i++) {
				JSONObject charData = charArray.getJSONObject(i);
				
				String baseId = charData.getString(JSON_API_BASE_ID);
				baseIdList.add(baseId);
				
				stmt.setString(1, baseId);
				stmt.setString(2, charData.getString(JSON_API_NAME_KEY));
				stmt.setString(3, lang.getSwgohCode());
				
				stmt.addBatch();
			}
			
			stmt.executeBatch();
			
			conn.commit();
			
			stmt.close();
			
			//Use language.English to avoid refreshing images multiple times
			if(Language.English.equals(lang)) {
				refreshCharImages(baseIdList);
			}
			
			future = api.getSupportData(SwgohAPI.Collection.abilityList,lang,new SwgohAPIFilter(JSON_API_ID,JSON_API_NAME_KEY));
			
			//We want this to be a synchronous method so we'll just wait on the completableFuture to complete...
			String abilitiesString = future.get();
			
			JSONArray abilitiesArray = new JSONArray(abilitiesString);
			
			//Store skills names into a Map to use later with skillList
			Map<String,String> abilitiesMap = new HashMap<String,String>();
			
			for(int i = 0 ; i < abilitiesArray.length() ; i++) {
				JSONObject abilityData = abilitiesArray.getJSONObject(i);
				
				abilitiesMap.put(abilityData.getString(JSON_API_ID), abilityData.getString(JSON_API_NAME_KEY));
			}
			
			future = api.getSupportData(SwgohAPI.Collection.skillList,lang,new SwgohAPIFilter(JSON_API_ABILITY_REFERENCE,JSON_API_ID));
			
			//We want this to be a synchronous method so we'll just wait on the completableFuture to complete...
			String skillsString = future.get();
			
			JSONArray skillsArray = new JSONArray(skillsString);
			
			stmt = conn.prepareStatement(SQL_INSERT_SKILLS);
			
			for(int i = 0 ; i < skillsArray.length() ; i++ ) {
				JSONObject skillData = skillsArray.getJSONObject(i);
				
				stmt.setString(1, skillData.getString(JSON_API_ID));
				String skillName = abilitiesMap.get(skillData.get(JSON_API_ABILITY_REFERENCE));
				stmt.setString(2, skillName);
				stmt.setString(3, lang.getSwgohCode());
				
				stmt.addBatch();
			}
			
			stmt.executeBatch();
			
			conn.commit();
			
			conn.setAutoCommit(true);
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		finally {
			try {
				if(stmt != null) {
					stmt.close();
				}
				if(conn != null) {
					conn.close();
				}
			}
			catch(SQLException e) {
				e.printStackTrace();
				logger.error(e.getMessage());
			}
		}
	}

	private void refreshCharImages(List<String> baseIdList) {
		
		String localDir = StaticVars.getCharPortraitLocalDir();
		
		if(!new File(localDir).exists()) {
			return;
		}
		
		try {
			for(String baseId : baseIdList) {
				BufferedImage image = StaticVars.getSwgohApi().getBufferedImage(new ToonImageRequestBuilder(baseId).build()).get();
				
				ImageIO.write(image, "png", new File(localDir+"/"+baseId+".png"));
			}
			
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		} catch (ExecutionException e) {
			
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
