package fr.jedistar;

import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.SQLException;

import org.json.JSONObject;

import com.mchange.v2.c3p0.ComboPooledDataSource;

import help.swgoh.api.SwgohAPI;
import help.swgoh.api.SwgohAPI.Language;
import help.swgoh.api.SwgohAPIBuilder;

public abstract class StaticVars {
	
	private static ComboPooledDataSource localDataSource;
	
	protected static JSONObject jsonSettings;
	protected static JSONObject jsonMessages;

	private static String jdbcUrl;
	private static String jdbcUser;
	private static String jdbcPasswd;
	
	private static SwgohAPI swgohApi;
	protected static Language lang;
	
	protected static boolean useCache = true;
	
	protected static String charPortraitLocalDir;
	protected static String charPortraitURI;

	public static Connection getJdbcConnection() throws SQLException {

		if(localDataSource == null) {
			initLocalDatasource();
		}

		return localDataSource.getConnection();
	}
	

	public static void setJdbcParams(String url, String user, String pwd) {
		jdbcUrl = url;
		jdbcPasswd = pwd;
		jdbcUser = user;
		
		initLocalDatasource();
	}
	
	private static void initLocalDatasource() {
		localDataSource = new ComboPooledDataSource();
		
		localDataSource.setJdbcUrl(jdbcUrl);
		localDataSource.setUser(jdbcUser);
		localDataSource.setPassword(jdbcPasswd);
		
		localDataSource.setDebugUnreturnedConnectionStackTraces(true);
		
		try {
			localDataSource.setDriverClass("com.mysql.jdbc.Driver");
		} catch (PropertyVetoException e) {
			e.printStackTrace();
		}
		
		localDataSource.setInitialPoolSize(10);
		localDataSource.setMinPoolSize(10);
		localDataSource.setAcquireIncrement(5);
		localDataSource.setMaxPoolSize(30);
		localDataSource.setTestConnectionOnCheckout(true);
	}
	
	public static void initSwgohApi(String user,String pass){
		swgohApi = new SwgohAPIBuilder()
				.withUsername(user)
				.withPassword(pass)
				.withDefaultLanguage(SwgohAPI.Language.English)
				.withDefaultEnums(false)
				.build();
	}
	
	public static SwgohAPI getSwgohApi() {
		return swgohApi;
	}


	public static JSONObject getJsonSettings() {
		return jsonSettings;
	}


	public static JSONObject getJsonMessages() {
		return jsonMessages;
	}


	public static Language getLang() {
		return lang;
	}


	public static boolean isUseCache() {
		return useCache;
	}


	public static String getCharPortraitLocalDir() {
		return charPortraitLocalDir;
	}


	public static String getCharPortraitURI() {
		return charPortraitURI;
	}
	
	
}
