package fr.jedistar;

import java.sql.SQLException;
import java.util.List;

import org.javacord.api.entity.message.Message;

import fr.jedistar.exception.AllyCodeNotFoundException;
import fr.jedistar.formats.CommandAnswer;

public interface JediStarBotCommand {
	
	public String getCommand();

	public CommandAnswer answer(List<String> params, Message receivedMessage, boolean isAdmin)
			throws AllyCodeNotFoundException,SQLException;
}
