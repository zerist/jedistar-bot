package fr.jedistar.exception;


public class AllyCodeNotFoundException extends Exception {

    private Integer allyCode;
    private Long discordID;

    private static final String MESSAGE_DISCORDID = "Ally code not found in database for discordID %d";
    private static final String MESSAGE = "Ally code %d not found in database.";

    public AllyCodeNotFoundException(Integer allyCode) {
        super(String.format(MESSAGE,allyCode));
        this.allyCode = allyCode;
    }

    public AllyCodeNotFoundException(Long discordID) {
        super(String.format(MESSAGE_DISCORDID,discordID));
        this.discordID = discordID;
    }

    public AllyCodeNotFoundException(Throwable cause, Integer allyCode) {
        super(String.format(MESSAGE,allyCode), cause);
        this.allyCode = allyCode;
    }

    public AllyCodeNotFoundException(Throwable cause, Long discordID) {
        super(String.format(MESSAGE_DISCORDID,discordID), cause);
        this.discordID = discordID;
    }

	public Long getDiscordID() {
		return discordID;
	}

	public Integer getAllyCode() {
		return allyCode;
	}    
    
    
}
