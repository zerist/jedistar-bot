package fr.jedistar.formats;

import java.util.ArrayList;

public class GuildAllyCodesList extends ArrayList<Integer>{

	private static final long serialVersionUID = 1L;
	
	private final String guildId;
	private  String rosterJson;
	
	
	public GuildAllyCodesList(String guildId) {
		super();
		this.guildId = guildId;
	}


	public String getGuildId() {
		return guildId;
	}


	public String getRosterJson() {
		return rosterJson;
	}


	public void setRosterJson(String rosterJson) {
		this.rosterJson = rosterJson;
	}

	
}
