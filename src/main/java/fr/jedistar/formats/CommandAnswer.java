package fr.jedistar.formats;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.javacord.api.entity.message.embed.EmbedBuilder;
import org.javacord.api.listener.message.reaction.ReactionAddListener;

import com.vdurmont.emoji.Emoji;


public class CommandAnswer {

	private String message = "";
	private EmbedBuilder embed;
	private List<Emoji> reactions;
	private List<PendingAction> pendingActions = new ArrayList<PendingAction>();
	private ReactionAddListener reactionAddListener;

	public CommandAnswer(String message, EmbedBuilder embed,Emoji...reactions) {
		if(message !=null) {
			this.message = message;
		}
		this.embed = embed;
		
		this.reactions = Arrays.asList(reactions);
	}

	public List<Emoji> getReactions() {
		return reactions;
	}

	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {

		if(message != null) {
			this.message = message;
		}
	}
	public EmbedBuilder getEmbed() {
		return embed;
	}
	public void setEmbed(EmbedBuilder embed) {
		this.embed = embed;
	}

	public List<PendingAction> getPendingActions() {
		return pendingActions;
	}
	
	public CommandAnswer addPendingActions(PendingAction...actions) {
		for(PendingAction action : actions) {
			pendingActions.add(action);
		}
		return this;
	}

	public ReactionAddListener getReactionAddListener() {
		return reactionAddListener;
	}

	public CommandAnswer setReactionAddListener(ReactionAddListener reactionAddListener) {
		this.reactionAddListener = reactionAddListener;
		return this;
	}

	
}
