package fr.jedistar;


import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.javacord.api.DiscordApi;
import org.javacord.api.DiscordApiBuilder;


import fr.jedistar.commands.PayoutCommand;
import fr.jedistar.listener.JediStarBotMessageListener;

public class JediStarBot {

	final Logger logger = LogManager.getLogger(this.getClass());

	DiscordApiBuilder builder;

	public JediStarBot(String inToken) {

		builder = new DiscordApiBuilder().setToken(inToken);



	}

	public void connect() {

		final JediStarBotMessageListener listener = new JediStarBotMessageListener();
		
		builder.setRecommendedTotalShards()
		.thenApply(DiscordApiBuilder::loginAllShards)
		.exceptionally(e-> {e.printStackTrace();
							logger.error(e.getMessage());
							return new ArrayList<CompletableFuture<DiscordApi>>();
		})
		.thenAccept(
			futures -> futures.forEach(
				future -> 	future.thenAccept(
					shard -> {
						shard.addMessageCreateListener(listener);
						shard.setMessageCacheSize(0,0);
					}
				)
			)
		);

	}


}