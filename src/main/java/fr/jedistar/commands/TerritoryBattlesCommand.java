package fr.jedistar.commands;

import java.awt.Color;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import org.json.JSONException;
import org.json.JSONObject;



import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.javacord.api.entity.message.Message;
import org.javacord.api.entity.message.embed.EmbedBuilder;

import fr.jedistar.JediStarBotCommand;
import fr.jedistar.StaticVars;
import fr.jedistar.commands.helper.GalaticPowerToStars;
import fr.jedistar.commands.helper.GalaticPowerToStars.StarInfo;
import fr.jedistar.commands.helper.StringFormating;
import fr.jedistar.exception.AllyCodeNotFoundException;
import fr.jedistar.formats.CommandAnswer;
import fr.jedistar.livedata.CharacterData;
import fr.jedistar.livedata.GuildData;
import fr.jedistar.utils.DbUtils;
import help.swgoh.api.SwgohAPI.Language;
import help.swgoh.api.exception.SwgohAPIDuplicateRequestException;
import help.swgoh.api.exception.SwgohAPIException;
import help.swgoh.api.exception.SwgohAPIRateLimitException;
import help.swgoh.api.exception.SwgohAPITimeoutException;
import fr.jedistar.livedata.Character;

public class TerritoryBattlesCommand implements JediStarBotCommand {

	private final Logger logger = LogManager.getLogger(this.getClass());

	private final String COMMAND;
	private final String COMMAND_PLATOON;
	private final String COMMAND_CHARS;
	private final String COMMAND_SHIPS;
	private final String COMMAND_STRATEGY;
	private final String COMMAND_MIN_STRATEGY;
	private final String COMMAND_DS_STRATEGY;
	private final String COMMAND_LS_STRATEGY;
	private final String COMMAND_REVERSE_ORDER;

	private final String HELP;

	private final String DISPLAYED_RESULTS;
	private final String DISPLAYED_RESULTS_REVERSE;
	private final String NO_UNIT_FOUND;
	private final String MARGIN_STARS_FROM_GP;
	private final String NEXT_STARS_FROM_GP;
	private final String DS_STARS_FROM_GP_TITLE;
	private final String LS_STARS_FROM_GP_TITLE;
	private final String MAX_STARS_FROM_GP_TITLE;
	private final String MIN_STARS_FROM_GP_TITLE;
	private final String MAX_STARS_FROM_GP;
	
	private final String ERROR_MESSAGE;
	private final String ERROR_MESSAGE_SQL;
	private final String ERROR_MESSAGE_PARAMS_NUMBER;
	private final String ERROR_COMMAND;
	private final String ERROR_INCORRECT_NUMBER;
	private final String TOO_MUCH_RESULTS;
	private final String MESSAGE_LOADING;
	private final String ERROR_SWGOH_API;
	private final String ERROR_SWGOH_API_RATE_LIMIT;
	private final String ERROR_SWGOH_API_PARTIAL_REFRESH;
	private final String ERROR_SWGOH_API_TIME_OUT;
	private final String ERROR_SWGOH_API_DUPLICATE_QUERY;

	private final static String SQL_FIND_GUILD_UNITS = "SELECT * FROM guildUnits WHERE guildID=? AND charID=? AND rarity>=? ORDER BY power LIMIT 15";
	private final static String SQL_FIND_GUILD_UNITS_REVERSE = "SELECT * FROM guildUnits WHERE guildID=? AND charID=? AND rarity>=? ORDER BY power DESC LIMIT 15";
	private final static String SQL_COUNT_GUILD_UNITS = "SELECT COUNT(*) as count FROM guildUnits WHERE guildID=? AND charID=? AND rarity>=?";
	private final static String SQL_SUM_GUILD_UNITS_GP ="SELECT SUM(u.power) as sumGP FROM guildUnits u WHERE guildID=? AND combatType=1";
	private final static String SQL_SUM_GUILD_SHIPS_GP = "SELECT SUM(u.power) as sumGP FROM guildUnits u WHERE guildID=? AND combatType=2";
	
	private final static String CHAR_MODE = "characters";
	private final static String SHIP_MODE = "ships";
	
	private final static Color EMBED_COLOR = Color.MAGENTA;
	
	private final static Integer MAX_RESULTS = 4;

	//Nom des champs dans le JSON
	private final static String JSON_ERROR_MESSAGE = "errorMessage";

	private final static String JSON_TB = "territoryBattlesCommandParams";

	private final static String JSON_TB_HELP = "help";

	private final static String JSON_TB_COMMANDS = "commands";
	private final static String JSON_TB_COMMANDS_BASE = "base";
	private final static String JSON_TB_COMMANDS_PLATOON = "platoon";
	private final static String JSON_TB_COMMANDS_CHARS = "characters";
	private final static String JSON_TB_COMMANDS_SHIPS = "ships";
	private final static String JSON_TB_COMMANDS_STRATEGY = "strategy";
	private final static String JSON_TB_COMMANDS_DS_STRATEGY = "dsStrategy";
	private final static String JSON_TB_COMMANDS_LS_STRATEGY = "lsStrategy";
	private final static String JSON_TB_COMMANDS_MIN_STRATEGY = "strategyMin";
	private final static String JSON_TB_COMMANDS_REVERSE_ORDER = "reverseOrder";
	
	private final static String JSON_TB_MESSAGES = "messages";
	private final static String JSON_TB_MESSAGES_DISPLAYED_RESULTS = "displayedResults";
	private final static String JSON_TB_MESSAGES_DISPLAYED_RESULTS_REVERSE = "displayedResultsReverse";
	private final static String JSON_TB_MESSAGES_NO_UNTI_FOUND = "noUnitFound";
	private final static String JSON_TB_MESSAGES_MAX_STARS_FROM_GP = "maxStarResult";
	private final static String JSON_TB_MESSAGES_DS_STARS_FROM_GP_TITLE = "dsBattleName";
	private final static String JSON_TB_MESSAGES_LS_STARS_FROM_GP_TITLE = "lsBattleName";
	private final static String JSON_TB_MESSAGES_MAX_STARS_FROM_GP_TITLE = "maxStarTitle";
	private final static String JSON_TB_MESSAGES_MIN_STARS_FROM_GP_TITLE = "minStarTitle";
	private final static String JSON_TB_MESSAGES_MARGIN_STARS_FROM_GP = "startCountMargin";
	private final static String JSON_TB_MESSAGES_NEXT_STARS_FROM_GP = "nextStar";

	private final static String JSON_TB_ERROR_MESSAGES = "errorMessages";
	private final static String JSON_TB_ERROR_MESSAGES_SQL = "sqlError";
	private final static String JSON_TB_ERROR_MESSAGES_PARAMS_NUMBER = "paramsNumber";
	private final static String JSON_TB_ERROR_MESSAGES_COMMAND = "commandError";
	private final static String JSON_TB_ERROR_MESSAGES_INCORRECT_NUMBER = "incorrectNumber";
	private final static String JSON_TB_TOO_MUCH_RESULTS = "tooMuchResults";
	
	private static final String JSON_SWGOH_API = "swgohApi";
	private static final String JSON_SWGOH_API_LOADING = "loadingFromSwgohApi";
	private static final String JSON_SWGOH_API_ERROR = "swgohApiError";
	private static final String JSON_SWGOH_API_RATE_LIMIT = "rateLimit";
	private static final String JSON_SWGOH_API_PARTIAL_REFRESH = "partialRefresh";
	private static final String JSON_SWGOH_API_TIME_OUT = "timeOut";
	private static final String JSON_SWGOH_API_DUPLICATE_QUERY = "duplicateQuery";

	public TerritoryBattlesCommand() {

		JSONObject parameters = StaticVars.getJsonMessages();

		ERROR_MESSAGE = parameters.getString(JSON_ERROR_MESSAGE);

		JSONObject tbParams = parameters.getJSONObject(JSON_TB);

		HELP = tbParams.getString(JSON_TB_HELP);

		JSONObject commands = tbParams.getJSONObject(JSON_TB_COMMANDS);
		COMMAND = commands.getString(JSON_TB_COMMANDS_BASE);
		COMMAND_PLATOON = commands.getString(JSON_TB_COMMANDS_PLATOON);
		COMMAND_CHARS = commands.getString(JSON_TB_COMMANDS_CHARS);
		COMMAND_SHIPS = commands.getString(JSON_TB_COMMANDS_SHIPS);
		COMMAND_STRATEGY = commands.getString(JSON_TB_COMMANDS_STRATEGY);
		COMMAND_DS_STRATEGY = commands.getString(JSON_TB_COMMANDS_DS_STRATEGY);
		COMMAND_LS_STRATEGY = commands.getString(JSON_TB_COMMANDS_LS_STRATEGY);
		COMMAND_MIN_STRATEGY = commands.getString(JSON_TB_COMMANDS_MIN_STRATEGY);
		COMMAND_REVERSE_ORDER = commands.getString(JSON_TB_COMMANDS_REVERSE_ORDER);

		JSONObject messages = tbParams.getJSONObject(JSON_TB_MESSAGES);
		DISPLAYED_RESULTS = messages.getString(JSON_TB_MESSAGES_DISPLAYED_RESULTS);
		DISPLAYED_RESULTS_REVERSE = messages.getString(JSON_TB_MESSAGES_DISPLAYED_RESULTS_REVERSE);
		NO_UNIT_FOUND = messages.getString(JSON_TB_MESSAGES_NO_UNTI_FOUND);
		MAX_STARS_FROM_GP = messages.getString(JSON_TB_MESSAGES_MAX_STARS_FROM_GP);
		LS_STARS_FROM_GP_TITLE = messages.getString(JSON_TB_MESSAGES_LS_STARS_FROM_GP_TITLE);
		DS_STARS_FROM_GP_TITLE = messages.getString(JSON_TB_MESSAGES_DS_STARS_FROM_GP_TITLE);
		MAX_STARS_FROM_GP_TITLE = messages.getString(JSON_TB_MESSAGES_MAX_STARS_FROM_GP_TITLE);
		MIN_STARS_FROM_GP_TITLE = messages.getString(JSON_TB_MESSAGES_MIN_STARS_FROM_GP_TITLE);
		MARGIN_STARS_FROM_GP = messages.getString(JSON_TB_MESSAGES_MARGIN_STARS_FROM_GP);
		NEXT_STARS_FROM_GP = messages.getString(JSON_TB_MESSAGES_NEXT_STARS_FROM_GP);
		
		JSONObject errorMessages = tbParams.getJSONObject(JSON_TB_ERROR_MESSAGES);
		ERROR_MESSAGE_SQL = errorMessages.getString(JSON_TB_ERROR_MESSAGES_SQL);
		ERROR_MESSAGE_PARAMS_NUMBER = errorMessages.getString(JSON_TB_ERROR_MESSAGES_PARAMS_NUMBER);
		ERROR_COMMAND = errorMessages.getString(JSON_TB_ERROR_MESSAGES_COMMAND);
		ERROR_INCORRECT_NUMBER = errorMessages.getString(JSON_TB_ERROR_MESSAGES_INCORRECT_NUMBER);
		TOO_MUCH_RESULTS = errorMessages.getString(JSON_TB_TOO_MUCH_RESULTS);
		
        JSONObject swgohApiParams = StaticVars.getJsonMessages().getJSONObject(JSON_SWGOH_API);
		MESSAGE_LOADING = swgohApiParams.getString(JSON_SWGOH_API_LOADING);
		ERROR_SWGOH_API = swgohApiParams.getString(JSON_SWGOH_API_ERROR);
		ERROR_SWGOH_API_RATE_LIMIT = swgohApiParams.getString(JSON_SWGOH_API_RATE_LIMIT);
		ERROR_SWGOH_API_PARTIAL_REFRESH = swgohApiParams.getString(JSON_SWGOH_API_PARTIAL_REFRESH);
		ERROR_SWGOH_API_TIME_OUT = swgohApiParams.getString(JSON_SWGOH_API_TIME_OUT);
		ERROR_SWGOH_API_DUPLICATE_QUERY = swgohApiParams.getString(JSON_SWGOH_API_DUPLICATE_QUERY);
	}

	@Override
	public String getCommand() {
		return COMMAND;
	}

	@Override
	public CommandAnswer answer(List<String> params, Message receivedMessage, boolean isAdmin) throws SQLException, AllyCodeNotFoundException {

		
		if(params.size() == 0) {
			return new CommandAnswer(ERROR_MESSAGE_PARAMS_NUMBER,null);
		}
		
		if(COMMAND_STRATEGY.equals(params.get(0))) {
			
			if(params.size() >  3 ) 
			{
				return new CommandAnswer(ERROR_COMMAND,null);
			}
			if(params.size() <  2)
			{
				return new CommandAnswer(error(ERROR_MESSAGE_PARAMS_NUMBER),null);
			}

				
				Integer allyCode = DbUtils.findAllyCode(receivedMessage.getUserAuthor().get());
				
				CompletableFuture<Message> loadingMessage = receivedMessage.getChannel().sendMessage(MESSAGE_LOADING);
				
				CompletableFuture<String> guildFuture = GuildData.getGuildIDFromAllyCode(allyCode);
								
				CompletableFuture<Void> executionFuture = guildFuture.thenAccept(guildID -> {
					
					
					CompletableFuture<Calendar> refreshFuture = GuildData.refreshGuildData(allyCode,guildID);
					
					CompletableFuture<Void> innerExecutionFuture = refreshFuture.thenAccept(refreshDate -> {
						
						loadingMessage.thenAccept(Message::delete);

						receivedMessage.getChannel().type();
						
						Calendar now = Calendar.getInstance();					
						now.add(Calendar.HOUR, -1);
						
						if(now.after(refreshDate)) {
							receivedMessage.getChannel().sendMessage(String.format(ERROR_SWGOH_API_PARTIAL_REFRESH, refreshDate));
						}				
						
						receivedMessage.getChannel().type();

						EmbedBuilder embed = new EmbedBuilder();
						embed.setColor(EMBED_COLOR);
						
						Integer characterGP = getGPSUM(guildID,CHAR_MODE);
						Integer shipGP = getGPSUM(guildID,SHIP_MODE);
																		
						Boolean isDark = null;
						String 	battleTitle = null;
						if(COMMAND_DS_STRATEGY.equals(params.get(1)))
						{
							isDark = true;
							battleTitle = DS_STARS_FROM_GP_TITLE;
						}
						else if(COMMAND_LS_STRATEGY.equals(params.get(1)))
						{
							isDark = false;
							battleTitle = LS_STARS_FROM_GP_TITLE;
						}
						else
						{
							receivedMessage.getChannel().sendMessage(ERROR_COMMAND);
						}
						
						GalaticPowerToStars strat = new GalaticPowerToStars(characterGP,shipGP,isDark);
						StarInfo starFromAir = strat.starFromShip;
						StarInfo starFromGround = strat.starFromCharacter;
						String 	strategyText = strat.strategy;
						String 	title = String.format(MAX_STARS_FROM_GP_TITLE,battleTitle);
						
						if(params.size() == 3 && COMMAND_MIN_STRATEGY.equals(params.get(2)))
						{
							starFromAir = strat.minStarFromShip;
							starFromGround =strat.minStarFromCharacter;
							strategyText =strat.minStrategy;
							title = String.format(MIN_STARS_FROM_GP_TITLE,battleTitle);
						}
						Integer totalStar = starFromAir.starCount+starFromGround.starCount;
						String airStarString = starFromAir.FormatToString(MARGIN_STARS_FROM_GP,NEXT_STARS_FROM_GP);
						String groundStarString = starFromGround.FormatToString(MARGIN_STARS_FROM_GP,NEXT_STARS_FROM_GP);
						String result = String.format(MAX_STARS_FROM_GP,StringFormating.formatNumber(characterGP), StringFormating.formatNumber(shipGP),StringFormating.formatNumber(shipGP+characterGP),airStarString,groundStarString,totalStar)+strategyText;
						embed.addField(title, result, true);
						receivedMessage.getChannel().sendMessage(embed);
						
					});
					
					innerExecutionFuture.exceptionally(e -> {
						Throwable sourceEx = e.getCause();
						
						sourceEx.printStackTrace();
						logger.error(sourceEx.getMessage());
						
						if(sourceEx instanceof SwgohAPIRateLimitException) {
							receivedMessage.getChannel().sendMessage(ERROR_SWGOH_API_RATE_LIMIT);
						}
						else if(sourceEx instanceof SwgohAPIDuplicateRequestException) {
							receivedMessage.getChannel().sendMessage(ERROR_SWGOH_API_DUPLICATE_QUERY);
						}
						else if(sourceEx instanceof SwgohAPITimeoutException) {
							receivedMessage.getChannel().sendMessage(ERROR_SWGOH_API_TIME_OUT);
						}
						else if(sourceEx instanceof SwgohAPIException || sourceEx instanceof JSONException) {
							receivedMessage.getChannel().sendMessage(ERROR_SWGOH_API);
						}
						else if(sourceEx instanceof NumberFormatException) {
							receivedMessage.getChannel().sendMessage(ERROR_INCORRECT_NUMBER);
						}
						
						loadingMessage.thenAccept(Message::delete);
						
						return null;
					});
				});
				
				executionFuture.exceptionally(e -> {
					Throwable sourceEx = e.getCause();

					sourceEx.printStackTrace();
					logger.error(sourceEx.getMessage());
					
					if(sourceEx instanceof SwgohAPIRateLimitException) {
						receivedMessage.getChannel().sendMessage(ERROR_SWGOH_API_RATE_LIMIT);
					}
					else if(sourceEx instanceof SwgohAPIDuplicateRequestException) {
						receivedMessage.getChannel().sendMessage(ERROR_SWGOH_API_DUPLICATE_QUERY);
					}
					else if(sourceEx instanceof SwgohAPITimeoutException) {
						receivedMessage.getChannel().sendMessage(ERROR_SWGOH_API_TIME_OUT);
					}
					else if(sourceEx instanceof SwgohAPIException || sourceEx instanceof JSONException) {
						receivedMessage.getChannel().sendMessage(ERROR_SWGOH_API);
					}
					
					loadingMessage.thenAccept(Message::delete);
					
					return null;
				});
				

		
			return null;

		}
		
			
		if(params.size() < 3) {
			return new CommandAnswer(error(ERROR_MESSAGE_PARAMS_NUMBER),null);
		}

		if(COMMAND_PLATOON.equals(params.get(0))) {

			Integer allyCode = DbUtils.findAllyCode(receivedMessage.getUserAuthor().get());
			
			CompletableFuture<Message> loadingMessage = receivedMessage.getChannel().sendMessage(MESSAGE_LOADING);
			
			CompletableFuture<String> guildFuture = GuildData.getGuildIDFromAllyCode(allyCode);
							
			CompletableFuture<Void> executionFuture = guildFuture.thenAccept(guildID -> {
				
				
				CompletableFuture<Calendar> refreshFuture = GuildData.refreshGuildData(allyCode,guildID);
				
				CompletableFuture<Void> innerExecutionFuture = refreshFuture.thenAccept(refreshDate -> {
					loadingMessage.thenAccept(Message::delete);
					
					receivedMessage.getChannel().type();
					
					Calendar now = Calendar.getInstance();					
					now.add(Calendar.HOUR, -1);
					
					if(now.after(refreshDate)) {
						receivedMessage.getChannel().sendMessage(String.format(ERROR_SWGOH_API_PARTIAL_REFRESH, refreshDate));
					}				
					
					receivedMessage.getChannel().type();
					
					boolean reverseOrder = false;
					Integer rarity = 0;
					try {
						String rarityAsString = params.get(params.size()-1).replace("*","");
						if(COMMAND_REVERSE_ORDER.equals(rarityAsString)) {
							reverseOrder=true;
						}
						else {
							rarity = Integer.parseInt(rarityAsString);
						}
					}
					catch(NumberFormatException e) {
						logger.warn(e.getMessage());
						receivedMessage.getChannel().sendMessage(error(ERROR_INCORRECT_NUMBER));
					}
					
					//recuperer le nom du perso si celui-ci contient des espaces
					int curIndex = 1;
					if(COMMAND_SHIPS.equalsIgnoreCase(params.get(1)) || COMMAND_CHARS.equalsIgnoreCase(params.get(1))) {
						curIndex = 2;
					}
					
					String unitName = params.get(curIndex);
					for(int i=curIndex+1 ; i<params.size()-1 ; i++) {
						unitName += " "+params.get(i);
					}
					
					String retour = error(ERROR_COMMAND);
					try {
						retour = findUnits(guildID, unitName,StaticVars.getLang(), rarity,receivedMessage,reverseOrder);	
					}
					catch(SQLException e) {
						logger.error(e.getMessage());
						e.printStackTrace();
						receivedMessage.getChannel().sendMessage(ERROR_MESSAGE_SQL);
						return;
					}
					receivedMessage.getChannel().sendMessage(retour);
					
				});
				
				innerExecutionFuture.exceptionally(e -> {
					Throwable sourceEx = e.getCause();
					
					sourceEx.printStackTrace();
					logger.error(sourceEx.getMessage());
					
					if(sourceEx instanceof SwgohAPIRateLimitException) {
						receivedMessage.getChannel().sendMessage(ERROR_SWGOH_API_RATE_LIMIT);
					}
					else if(sourceEx instanceof SwgohAPIDuplicateRequestException) {
						receivedMessage.getChannel().sendMessage(ERROR_SWGOH_API_DUPLICATE_QUERY);
					}
					else if(sourceEx instanceof SwgohAPITimeoutException) {
						receivedMessage.getChannel().sendMessage(ERROR_SWGOH_API_TIME_OUT);
					}
					else if(sourceEx instanceof SwgohAPIException || sourceEx instanceof JSONException) {
						receivedMessage.getChannel().sendMessage(ERROR_SWGOH_API);
					}
					else if(sourceEx instanceof NumberFormatException) {
						receivedMessage.getChannel().sendMessage(ERROR_INCORRECT_NUMBER);
					}
					
					loadingMessage.thenAccept(Message::delete);
					
					return null;
				});
			});
			
			executionFuture.exceptionally(e -> {
				Throwable sourceEx = e.getCause();

				sourceEx.printStackTrace();
				logger.error(sourceEx.getMessage());
				
				if(sourceEx instanceof SwgohAPIRateLimitException) {
					receivedMessage.getChannel().sendMessage(ERROR_SWGOH_API_RATE_LIMIT);
				}
				else if(sourceEx instanceof SwgohAPIDuplicateRequestException) {
					receivedMessage.getChannel().sendMessage(ERROR_SWGOH_API_DUPLICATE_QUERY);
				}
				else if(sourceEx instanceof SwgohAPITimeoutException) {
					receivedMessage.getChannel().sendMessage(ERROR_SWGOH_API_TIME_OUT);
				}
				else if(sourceEx instanceof SwgohAPIException || sourceEx instanceof JSONException) {
					receivedMessage.getChannel().sendMessage(ERROR_SWGOH_API);
				}
				
				loadingMessage.thenAccept(Message::delete);
				
				return null;
			});
			

	
		return null;
			
			
		}

		return new CommandAnswer(error(ERROR_COMMAND),null);
	}


	
	private Integer getGPSUM(String guildID,String mode) 
	{

		Integer result = -1;
		String request = "";


		if(SHIP_MODE.equals(mode)) {
			request=SQL_SUM_GUILD_SHIPS_GP;
		}

		if(CHAR_MODE.equals(mode)) {
			request=SQL_SUM_GUILD_UNITS_GP;
		}
			
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {
			conn = StaticVars.getJdbcConnection();

			stmt = conn.prepareStatement(request);
			
			stmt.setString(1,guildID);
			
			logger.debug("Executing query : "+stmt.toString());
			rs = stmt.executeQuery();
			
			rs.next();
			
			result = rs.getInt("sumGP");
				
		}
		catch(SQLException e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			return -1;
		}
		finally {
			try {
				if(rs != null) {
					rs.close();
				}
				if(stmt != null) {
					stmt.close();
				}
				if(conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				logger.error(e.getMessage());
			}
		}
		
		return result;
	}

	private String findUnits(String guildID,String charName,Language lang,Integer rarity,Message receivedMessage,boolean reverseOrder) throws SQLException {
		
		List<Character> charsList = CharacterData.findMatchingCharacters(charName, lang);
		
		if(charsList == null) {
			return ERROR_MESSAGE_SQL;
		}
		
		if(charsList.isEmpty()) {
			return "No character found with this name, approximate matching will come in a future update";
		}
		
		if(charsList.size() > MAX_RESULTS) {
			String returnStr = TOO_MUCH_RESULTS;
			
			for(Character chara : charsList) {
				returnStr += chara.name + "\r\n";
			}
			
			return returnStr;
		}
		
		for(Character chara : charsList) {
			EmbedBuilder embed = new EmbedBuilder();
			embed.setAuthor(chara.name,"",chara.image);
			embed.setThumbnail(chara.image);
			embed.setColor(EMBED_COLOR);
			
			Connection conn = null;
			PreparedStatement stmt = null;
			ResultSet rs = null;

			try {
				conn = StaticVars.getJdbcConnection();

				stmt = conn.prepareStatement(SQL_COUNT_GUILD_UNITS);
				
				stmt.setString(1,guildID);
				stmt.setString(2, chara.baseID);
				stmt.setInt(3, rarity);
				
				rs = stmt.executeQuery();
				
				rs.next();
				
				Integer totalMatchNumber = rs.getInt("count");
				if(totalMatchNumber == 0) {
					embed.setTitle(NO_UNIT_FOUND);
				}
				if(totalMatchNumber > 15) {
					String message = reverseOrder ? DISPLAYED_RESULTS_REVERSE : DISPLAYED_RESULTS;
					embed.setTitle(String.format(message, totalMatchNumber));
				}
				
				rs.close();
				stmt.close();
				
				if(totalMatchNumber > 0) {
					String query = reverseOrder ? SQL_FIND_GUILD_UNITS_REVERSE : SQL_FIND_GUILD_UNITS;
					stmt = conn.prepareStatement(query);

					stmt.setString(1,guildID);
					stmt.setString(2, chara.baseID);
					stmt.setInt(3, rarity);

					logger.debug("Executing query : "+stmt.toString());

					rs = stmt.executeQuery();

					//Utilisation de LinkedHashMap pour conserver l'ordre
					Map<Integer,String> contentPerRarity = new LinkedHashMap<Integer,String>();

					//population de la table dans le bon ordre
					if(!reverseOrder) {
						for(int i=rarity;i<=7;i++) {
							contentPerRarity.put(i, "");
						}
					}
					else {
						for(int i=7 ; i>=rarity && i>0 ; i--) {
							contentPerRarity.put(i, "");
						}
					}
					

					while(rs.next()) {

						Integer currRarity = rs.getInt("rarity");

						String currentContent = contentPerRarity.get(currRarity);
						Integer intPower = rs.getInt("power");
						String power = NumberFormat.getIntegerInstance().format(intPower);
						currentContent += power +" GP - "+rs.getString("player")+" \r\n";
						contentPerRarity.put(currRarity,currentContent);
					}
					
					for(Map.Entry<Integer, String> entry : contentPerRarity.entrySet()) {
						if(!StringUtils.isEmpty(entry.getValue())) {
							embed.addField(entry.getKey()+"*",entry.getValue(),true);
						}
					}
				}
				embed.addField("-", "Data from [swgoh.help](https://swgoh.help)\r\nBot designed by [the french community Dark Side Of The Moon (DSOTM)](https://jedistar.jimdo.com)", false);
				receivedMessage.getChannel().sendMessage(null, embed);
			}
			catch(SQLException e) {
				logger.error(e.getMessage());
				e.printStackTrace();
				return ERROR_MESSAGE_SQL;
			}
			finally {
				try {
					if(rs != null) {
						rs.close();
					}
					if(stmt != null) {
						stmt.close();
					}
					if(conn != null) {
						conn.close();
					}
				} catch (SQLException e) {
					logger.error(e.getMessage());
				}
			}
		}
		return null;
	}
	
	
	
	private String error(String message) {
		return ERROR_MESSAGE +"**"+ message + "**\r\n\r\n"+ HELP;
	}
	
	
}
