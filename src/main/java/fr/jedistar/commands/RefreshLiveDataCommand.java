package fr.jedistar.commands;

import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.apache.commons.lang3.StringUtils;
import org.javacord.api.entity.message.Message;

import fr.jedistar.JediStarBotCommand;
import fr.jedistar.exception.AllyCodeNotFoundException;
import fr.jedistar.formats.CommandAnswer;
import fr.jedistar.livedata.LiveDataRefresher;
import help.swgoh.api.SwgohAPI;
import help.swgoh.api.SwgohAPI.Language;

public class RefreshLiveDataCommand implements JediStarBotCommand {

	@Override
	public String getCommand() {
		return "refreshlivedata";
	}

	@Override
	public CommandAnswer answer(List<String> params, Message receivedMessage, boolean isAdmin)
			throws AllyCodeNotFoundException, SQLException {
		
		if(receivedMessage.getAuthor().getId() != 341475291467874307L) {
			return null;
		}
		
		Language selectedLanguage = null;
		
		for(Language lang : SwgohAPI.Language.values()) {
			if(StringUtils.containsIgnoreCase(lang.name(), params.get(0))) {
				selectedLanguage = lang;
				break;
			}
		}
		
		if(selectedLanguage == null) {
			receivedMessage.getChannel().sendMessage("Wrong language");
		}
		
		CompletableFuture<Message> message = receivedMessage.getChannel().sendMessage("Loading live data...");
		
		CompletableFuture<Void> future = CompletableFuture.runAsync(new LiveDataRefresher(selectedLanguage));
		
		future.thenAccept(Void -> {
			message.thenAccept(Message::delete);
			receivedMessage.getChannel().sendMessage("Live data refresh over.");
		}
		);
		
		
		
		
		return null;
	}

}
