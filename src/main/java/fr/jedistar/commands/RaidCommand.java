package fr.jedistar.commands;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.javacord.api.entity.message.Message;
import fr.jedistar.JediStarBotCommand;
import fr.jedistar.StaticVars;
import fr.jedistar.formats.CommandAnswer;

public class RaidCommand implements JediStarBotCommand {
	
	private final Logger logger = LogManager.getLogger(this.getClass());

	private final String COMMAND;

	private static String MESSAGE_PERCENTS_DIFFERENCE;
	private static String MESSAGE_PERCENTS_DIFFERENCE_PHASECHANGE;
	private static String MESSAGE_DAMAGES;
	private static String MESSAGE_PERCENT;
	private static String MESSAGE_TARGET;
	private static String MESSAGE_TARGET_RANGE;
	
	private static String ERROR_MESSAGE ;
	private static String HELP;
	
	private static String OBJECTIVE_OVER_RAID_END;
	private static String OBJECTIVE_OVER_RAID_END_SECOND;
	private static String INCOHERENT_PARAMETERS;
	private static String INCORRECT_NUMBER;
	private static String PHASE_NOT_FOUND;
	private static String INCORRECT_PARAMS_NUMBER;
	private static String RAID_NOT_FOUND;
			
	//Représente 1% de HP pour les différentes phases des différents raids
	private Map<String,LinkedHashMap<String,Integer>> phaseHPmap;
	private Map<String,List<String>> aliasesMap;
	
	//Variables JSON
	private final static String JSON_ERROR_MESSAGE = "errorMessage";
	private final static String JSON_RAID_COMMAND = "raidCommandParameters";
	private final static String JSON_RAID_COMMAND_COMMAND = "command";
	private final static String JSON_MESSAGES = "messages";
	private final static String JSON_MESSAGE_PERCENTS_DIFFERENCE = "percentDifference";
	private final static String JSON_MESSAGE_PERCENTS_DIFFERENCE_PHASE_CHANGE = "percentDifferencePhaseChange";
	private final static String JSON_MESSAGE_DAMAGES = "damages";
	private final static String JSON_MESSAGE_PERCENT = "percent";
	private final static String JSON_MESSAGE_TARGET = "target";
	private final static String JSON_MESSAGE_HELP = "help";
	private final static String JSON_MESSAGE_TARGET_RANGE = "targetRange";
	
	private final static String JSON_ERROR_MESSAGES = "errorMessages";
	private final static String JSON_ERROR_MESSAGE_OVER_RAID_END = "overRaidEnd";
	private final static String JSON_ERROR_MESSAGE_OVER_RAID_END_SECOND = "overRaidEndSecond";
	private final static String JSON_ERROR_MESSAGE_INCOHERENT_PARAMS = "incoherentParams";
	private final static String JSON_ERROR_MESSAGE_INCORRECT_NUMBER = "incorrectNumber";
	private final static String JSON_ERROR_MESSAGE_PHASE_NOT_FOUND = "phaseNotFound";
	private final static String JSON_ERROR_MESSAGE_INCORRECT_PARAMS_NUMBER = "incorrectParamsNumber";
	private final static String JSON_ERROR_MESSAGE_RAID_NOT_FOUND = "raidNotFound";

	private final static String JSON_RAIDS = "raids";
	private final static String JSON_RAID_NAME = "name";
	private final static String JSON_RAID_ALIASES = "aliases";
	private final static String JSON_RAID_PHASES = "phases";
	private final static String JSON_RAID_PHASE_NAME = "name";
	private final static String JSON_RAID_PHASE_DAMAGE = "damage1percent";

	private class RaidHealth {

		public String phase;
		public float healthPercentage;
		
		public RaidHealth(String p,float h) 
		{
			phase = p;
			healthPercentage = h;
		}
	}
	
	public RaidCommand() {
		super();


		JSONObject parameters = StaticVars.getJsonMessages();

		//messages de base
		ERROR_MESSAGE = parameters.getString(JSON_ERROR_MESSAGE);

		//Param�tres propres � l'�quilibrage
		JSONObject raidParams = parameters.getJSONObject(JSON_RAID_COMMAND);

		COMMAND = raidParams.getString(JSON_RAID_COMMAND_COMMAND);

		//Messages
		JSONObject messages = raidParams.getJSONObject(JSON_MESSAGES);
		MESSAGE_PERCENTS_DIFFERENCE = messages.getString(JSON_MESSAGE_PERCENTS_DIFFERENCE);
		MESSAGE_PERCENTS_DIFFERENCE_PHASECHANGE = messages.getString(JSON_MESSAGE_PERCENTS_DIFFERENCE_PHASE_CHANGE);
		MESSAGE_DAMAGES = messages.getString(JSON_MESSAGE_DAMAGES);
		MESSAGE_PERCENT = messages.getString(JSON_MESSAGE_PERCENT);
		MESSAGE_TARGET = messages.getString(JSON_MESSAGE_TARGET);
		MESSAGE_TARGET_RANGE = messages.getString(JSON_MESSAGE_TARGET_RANGE);
		HELP = messages.getString(JSON_MESSAGE_HELP);

		//Messages d'erreur
		JSONObject errorMessages = raidParams.getJSONObject(JSON_ERROR_MESSAGES);
		OBJECTIVE_OVER_RAID_END = errorMessages.getString(JSON_ERROR_MESSAGE_OVER_RAID_END);
		OBJECTIVE_OVER_RAID_END_SECOND = errorMessages.getString(JSON_ERROR_MESSAGE_OVER_RAID_END_SECOND);
		INCOHERENT_PARAMETERS = errorMessages.getString(JSON_ERROR_MESSAGE_INCOHERENT_PARAMS);
		INCORRECT_NUMBER = errorMessages.getString(JSON_ERROR_MESSAGE_INCORRECT_NUMBER);
		PHASE_NOT_FOUND = errorMessages.getString(JSON_ERROR_MESSAGE_PHASE_NOT_FOUND);
		INCORRECT_PARAMS_NUMBER = errorMessages.getString(JSON_ERROR_MESSAGE_INCORRECT_PARAMS_NUMBER);
		RAID_NOT_FOUND = errorMessages.getString(JSON_ERROR_MESSAGE_RAID_NOT_FOUND);

		//gestion des raids
		JSONArray raids = raidParams.getJSONArray(JSON_RAIDS);
		phaseHPmap = new HashMap<String, LinkedHashMap<String,Integer>>();
		aliasesMap = new HashMap<String,List<String>>();

		for(int r=0 ; r<raids.length() ; r++) {
			LinkedHashMap<String,Integer> phasesHPmapForThisRaid = new LinkedHashMap<String,Integer>();

			JSONObject raid = raids.getJSONObject(r);

			String raidName = raid.getString(JSON_RAID_NAME);
			JSONArray phases = raid.getJSONArray(JSON_RAID_PHASES);

			for(int p=0 ; p<phases.length() ; p++) {
				JSONObject phase = phases.getJSONObject(p);

				String phaseName = phase.getString(JSON_RAID_PHASE_NAME);
				Integer phaseDamage = phase.getInt(JSON_RAID_PHASE_DAMAGE);
				
				phasesHPmapForThisRaid.put(phaseName, phaseDamage);
			}

			phaseHPmap.put(raidName, phasesHPmapForThisRaid);

			JSONArray aliases = raid.getJSONArray(JSON_RAID_ALIASES);
			List<String> aliasesForThisRaid = new ArrayList<String>();

			for(int a=0 ; a<aliases.length() ; a++) {
				aliasesForThisRaid.add(aliases.getString(a));
			}

			aliasesMap.put(raidName, aliasesForThisRaid);
		}
	}

	
	public CommandAnswer answer(List<String> params,Message messageRecu,boolean isAdmin) {

		if(params.size() == 0) {
			return new CommandAnswer(HELP,null);
		}
		String raidName = params.get(0);
		
		//Accepter des noms alternatifs
		for(Entry<String, List<String>> raidAliases : aliasesMap.entrySet()) {
			String currentRaidName = raidAliases.getKey();
			for(String alias : raidAliases.getValue()) {
				raidName = raidName.replaceAll(alias, currentRaidName);
			}
		}
		
		if(phaseHPmap.get(raidName) == null) {
			return error(RAID_NOT_FOUND);
		}
		
		try {		
			String phaseName = params.get(1).toLowerCase();

			if(!phaseHPmap.get(raidName).containsKey(phaseName)) {
				return error(PHASE_NOT_FOUND);
			}
			
			if(params.size() == 3) {			
				return doPhaseWithOneParameter(params.get(2), raidName, phaseName);		
			}
			else if(params.size() == 4) {
				return doPhaseWithTwoParameters(params.get(2),params.get(3), raidName, phaseName);		
			}
			else if(params.size() == 5) {
				return doPhaseWithThreeParameters(params.get(2),params.get(3),params.get(4), raidName, phaseName);		
			}
			else {
				return error(INCORRECT_PARAMS_NUMBER);
			}
		}
		catch( IndexOutOfBoundsException e) {
			return error(PHASE_NOT_FOUND);
		}
		
		
	}
	
	private Float formatNumberParameters(String value)
	{
		value = value.replace("%", "");
		value = value.replace(",", ".");
		Integer multiplier = 1;
		if(value.endsWith("k")) {
			value = value.replace("k","");
			multiplier = 1000;
		}
		if(value.endsWith("m")) {
			value = value.replace("m","");
			multiplier = 1000000;
		}
		
		Float valueAsFloat = Float.valueOf(value) * multiplier;
		if(valueAsFloat < 0) {
			valueAsFloat = -1 * valueAsFloat;
		}
		return valueAsFloat;
		
		
	}

	private CommandAnswer doPhaseWithOneParameter(String value, String raidName,String phaseName) {
		
		Integer phaseHP1percent = phaseHPmap.get(raidName).get(phaseName);		
		if(phaseHP1percent == null) {
			return error(PHASE_NOT_FOUND);
		}

		try {
			
			Float valueAsFloat = formatNumberParameters(value);
			
			if(valueAsFloat <= 100) {
				//Il s'agit d'un pourcentage
				Integer responseValue = (int) (valueAsFloat * phaseHP1percent);
				
				String formattedValue = NumberFormat.getIntegerInstance().format(responseValue);

				String message = String.format(MESSAGE_PERCENT, raidName,phaseName,value,formattedValue);
				return new CommandAnswer(message, null);
			}
			else {
				//Il s'agit d'une valeur en d�g�ts
				Float responseValue = valueAsFloat / phaseHP1percent;
				
				String formattedValue = NumberFormat.getIntegerInstance().format(valueAsFloat.intValue());
				
				String message = String.format(MESSAGE_DAMAGES,raidName,phaseName,formattedValue,responseValue);
				return new CommandAnswer(message,null);
			}
		}

		catch(NumberFormatException e) {
			return error(INCORRECT_NUMBER);
		}
	}


	private CommandAnswer doPhaseWithTwoParameters(String value, String secondValue, String raidName,String phaseName) {
		
		try {
			Float valueAsFloat = formatNumberParameters(value);
			Float secondValueAsFloat = formatNumberParameters(secondValue);
			
			if(valueAsFloat <= 100 && secondValueAsFloat < 100) {
				//Il s'agit de deux pourcentages�
				return doPhaseWithTwoPercentages(valueAsFloat, secondValueAsFloat, raidName, phaseName);
			}
			
			if(valueAsFloat <= 100 && secondValueAsFloat > 100) {
				//Il s'agit d'un pourcentage et d'une valeur
				return doPhaseWithPercentageAndValue(valueAsFloat,secondValueAsFloat,raidName,phaseName);
			}
			if(secondValueAsFloat <= 100 && valueAsFloat > 100) {
				//Il s'agit d'un pourcentage et d'une valeur
				return doPhaseWithPercentageAndValue(secondValueAsFloat,valueAsFloat,raidName,phaseName);
			}
			
			return error(INCOHERENT_PARAMETERS);
		}
		catch(NumberFormatException e) {
			return error(INCORRECT_NUMBER);
		}
	}
	
	
	
	private CommandAnswer doPhaseWithThreeParameters(String value, String secondValue, String thirdValue, String raidName,String phaseName)
	{

		try {
			Float valueAsFloat = formatNumberParameters(value);
			Float secondValueAsFloat = formatNumberParameters(secondValue);
			Float thirdValueAsFloat = formatNumberParameters(thirdValue);
			
			if(valueAsFloat <= 100 && secondValueAsFloat > 100 && thirdValueAsFloat > 100 && thirdValueAsFloat > secondValueAsFloat) {
				//Il s'agit d'un pourcentage et d'une valeur
				return doPhaseWithRange(valueAsFloat,secondValueAsFloat,thirdValueAsFloat,raidName,phaseName);
			}
			
			return error(INCOHERENT_PARAMETERS);
		}
		catch(NumberFormatException e) {
			return error(INCORRECT_NUMBER);
		}
	}

	private CommandAnswer doPhaseWithTwoPercentages(Float valueAsFloat, Float secondValueAsFloat, String raidName,
			String phaseName) {
		
		Integer phaseHP1percent = phaseHPmap.get(raidName).get(phaseName);		
		if(phaseHP1percent == null) {
			return error(PHASE_NOT_FOUND);
		}
		
		Float paramValue = valueAsFloat - secondValueAsFloat;

		if(paramValue < 0) {
			//Il y a eu changement de phase
			
			Integer nextPhaseHP1percent = phaseHPmap.get(raidName).get(phaseName);		
			if(nextPhaseHP1percent == null) {
				return error(INCOHERENT_PARAMETERS);
			}
			
			//D�g�ts faits � la fin de la phase annonc�e
			Integer responseValue = (int) (valueAsFloat * phaseHP1percent);
			
			//On ajoute les d�g�ts faits au d�but de la phase suivante
			responseValue += (int) (100 - secondValueAsFloat) * nextPhaseHP1percent;
			
			String formattedValue = NumberFormat.getIntegerInstance().format(responseValue);
			String message = String.format(MESSAGE_PERCENTS_DIFFERENCE_PHASECHANGE,
					valueAsFloat,
					secondValueAsFloat,
					raidName,
					phaseName,
					formattedValue);
			return new CommandAnswer(message,null);
		}
		else {
			Integer responseValue = (int) (paramValue * phaseHP1percent);
			String formattedValue = NumberFormat.getIntegerInstance().format(responseValue);

			String message = String.format(MESSAGE_PERCENTS_DIFFERENCE,
					valueAsFloat,
					secondValueAsFloat,
					raidName,
					phaseName,
					formattedValue);
			return new CommandAnswer(message,null);
		}
	}

	private CommandAnswer doPhaseWithPercentageAndValue(Float initialPercentage, Float targetValue,String raidName, String phaseName) {
		
		RaidHealth ValueResult = calculateTargetPhaseAndHealth(initialPercentage, targetValue,raidName, phaseName);
		
		if(ValueResult.phase == null)
		{
			return new CommandAnswer(OBJECTIVE_OVER_RAID_END,null);
		}
		
		String formattedValue = NumberFormat.getIntegerInstance().format(targetValue);
		String message = String.format(MESSAGE_TARGET, raidName,phaseName,initialPercentage,
								formattedValue,ValueResult.phase,ValueResult.healthPercentage) ;
		return new CommandAnswer(message,null);
	}
	
	private RaidHealth calculateTargetPhaseAndHealth(Float initialPercentage, Float targetValue,String raidName, String phaseName) {
		
		LinkedHashMap<String,Integer> phaseHPmapForCurrentRaid = phaseHPmap.get(raidName);
		
		Collection<Integer> phasesValuesList = phaseHPmapForCurrentRaid.values();
		
		Float resultPercentage = (float)0;
		
		String phaseNameCursor = phaseName;
		
		Iterator<String> phasesNames = phaseHPmapForCurrentRaid.keySet().iterator();
		
		//Advance the iterator to the current phase
		while(phasesNames.hasNext()) {
			if(phasesNames.next().equals(phaseName)) {
				break;
			}
		}
		
		Float residualDamage = targetValue;
		
		Float currentPhasePercentage = initialPercentage;
		
		while(residualDamage > 0) {
			
			Integer HP1percent = phaseHPmapForCurrentRaid.get(phaseNameCursor);
			
			if(HP1percent == null) {
				return new RaidHealth(null,0);
			}
			
			Float requiredPercentage = residualDamage / HP1percent;
			
			if(requiredPercentage < currentPhasePercentage) {
				resultPercentage = currentPhasePercentage - requiredPercentage;
				residualDamage = (float) 0;
			}
			else {
				residualDamage -= currentPhasePercentage * HP1percent;
				
				if(phasesNames.hasNext()) {
					phaseNameCursor = phasesNames.next();
				}
				else {
					return new RaidHealth(null,0);
				}
				currentPhasePercentage = (float) 100;
			}
		}
		
		return new RaidHealth(phaseNameCursor,resultPercentage);
	}
	
	
	private CommandAnswer doPhaseWithRange(Float initialPercentage, Float targetValueMin, Float targetValueMax,String raidName, String phaseName) {
		
		RaidHealth minValueResult = calculateTargetPhaseAndHealth(initialPercentage, targetValueMin,raidName, phaseName);
		RaidHealth maxValueResult = calculateTargetPhaseAndHealth(initialPercentage, targetValueMax,raidName, phaseName);
		
		String formattedValueMin = NumberFormat.getIntegerInstance().format(targetValueMin);
		String formattedValueMax = NumberFormat.getIntegerInstance().format(targetValueMax);
		
		if(minValueResult.phase == null)
		{
			return new CommandAnswer(OBJECTIVE_OVER_RAID_END,null);
		}
		else if (maxValueResult.phase == null)
		{
			String message = String.format(MESSAGE_TARGET, raidName,phaseName,initialPercentage,
					formattedValueMin,minValueResult.phase,minValueResult.healthPercentage) + OBJECTIVE_OVER_RAID_END_SECOND;
				return new CommandAnswer(message,null);
		}
		
		String message = String.format(MESSAGE_TARGET_RANGE, raidName,phaseName,initialPercentage,
				formattedValueMin,formattedValueMax,minValueResult.phase,minValueResult.healthPercentage,maxValueResult.phase,maxValueResult.healthPercentage);
		
		return new CommandAnswer(message,null);
	}
	
	private CommandAnswer error(String errorMessage) {
		String message = ERROR_MESSAGE +"**"+ errorMessage + "**\r\n\r\n"+ HELP;
		
		return new CommandAnswer(message, null);
	}

	@Override
	public String getCommand() {
		return COMMAND;
	}
}
