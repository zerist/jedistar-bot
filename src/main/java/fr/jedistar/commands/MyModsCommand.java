package fr.jedistar.commands;

import org.javacord.api.entity.message.Message;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import fr.jedistar.JediStarBotCommand;
import fr.jedistar.StaticVars;
import fr.jedistar.exception.AllyCodeNotFoundException;
import fr.jedistar.formats.CommandAnswer;
import fr.jedistar.utils.DbUtils;
import org.json.JSONObject;

import java.awt.*;
import java.sql.SQLException;
import java.util.List;

public class MyModsCommand implements JediStarBotCommand {


    private final String COMMAND;

    private final String EMBED_TITLE;
    private final String EMBED_CONTENT;

    private final String URL_MODS_MANAGER;


    //JSON variables
    private final static String JSON_MYMODS = "mymodsCommandParameters";
    private final static String JSON_COMMANDS = "commands";
    private final static String JSON_COMMANDS_BASE = "base";
    private final static String JSON_RANCOR_URL = "rancorModsManagerURL";
    private final static String JSON_MESSAGES = "messages";
    private final static String JSON_MESSAGES_EMBED_TITLE = "embedTitle";
    private final static String JSON_MESSAGES_EMBED_CONTENT = "embedContent";

    @Override
    public String getCommand() {
        return COMMAND;
    }

    public MyModsCommand() {

        JSONObject params = StaticVars.getJsonMessages().getJSONObject(JSON_MYMODS);

        JSONObject commands = params.getJSONObject(JSON_COMMANDS);
        COMMAND = commands.getString(JSON_COMMANDS_BASE);

        URL_MODS_MANAGER = params.getString(JSON_RANCOR_URL);

        JSONObject messages = params.getJSONObject(JSON_MESSAGES);
        EMBED_TITLE = messages.getString(JSON_MESSAGES_EMBED_TITLE);
        EMBED_CONTENT = messages.getString(JSON_MESSAGES_EMBED_CONTENT);

    }

    @Override
    public CommandAnswer answer(List<String> params, Message receivedMessage, boolean isAdmin) throws AllyCodeNotFoundException, SQLException {

    	if(!receivedMessage.getUserAuthor().isPresent()) {
    		return null;
    	}
    	
        Integer allyCode = DbUtils.findAllyCode(receivedMessage.getUserAuthor().get());

        EmbedBuilder embed = new EmbedBuilder();

        String title = String.format(EMBED_TITLE,receivedMessage.getAuthor().getName());
        String url = String.format(URL_MODS_MANAGER,allyCode);

        embed.setTitle(title);
        embed.setUrl(url);
        embed.setColor(Color.WHITE);
        embed.setDescription(EMBED_CONTENT);

        embed.setAuthor(receivedMessage.getAuthor());

        return new CommandAnswer(null,embed);
    }
}
