/**
 * 
 */
package fr.jedistar.commands;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.javacord.api.entity.message.Message;
import org.javacord.api.entity.user.User;
import org.json.JSONObject;


import com.vdurmont.emoji.Emoji;
import com.vdurmont.emoji.EmojiManager;

import fr.jedistar.JediStarBotCommand;
import fr.jedistar.StaticVars;
import fr.jedistar.formats.CommandAnswer;
import fr.jedistar.listener.specific.PlayerAllyCodeInserter;

/**
 * @author Jerem
 *
 */
public class SetUpCommand implements JediStarBotCommand {

	private final Logger logger = LogManager.getLogger(this.getClass());

	private final String COMMAND;
	private final String COMMAND_GUILD_NUMBER;
	private final String COMMAND_ALLY_CODE;

	private final String CONFIRM_UPDATE_ALLY_CODE;
	private final String SETUP_ALLY_CODE_OK;

	private final String ERROR_MESSAGE;
	private final String HELP;
	private final String FORBIDDEN;
	private final String PARAMS_NUMBER;
	private final String NUMBER_PROBLEM;
	private final String SQL_ERROR;	
	private final String NO_COMMAND_FOUND;
	
	private static final String SELECT_PLAYER = "SELECT * FROM player WHERE discordUserID=?";

	//Nom des champs JSON
	private final static String JSON_ERROR_MESSAGE = "errorMessage";

	private static final String JSON_SETUP = "setUpCommandParameters";

	private static final String JSON_SETUP_HELP = "help";

	private static final String JSON_SETUP_COMMANDS = "commands";
	private static final String JSON_SETUP_COMMANDS_BASE = "base";
	private static final String JSON_SETUP_COMMANDS_GUILD_NUMBER = "guildNumber";
	private static final String JSON_SETUP_COMMANDS_ALLY_CODE = "allyCode";


	private static final String JSON_SETUP_MESSAGES = "messages";
	private static final String JSON_SETUP_MESSAGES_CONFIRM_MESSAGE_ALLY_CODE = "confirmUpdateAllyCode";
	private static final String JSON_SETUP_MESSAGES_ALLY_CODE_SETUP_OK = "allyCodeSetupOK";

	private static final String JSON_SETUP_ERROR_MESSAGES = "errorMessages";
	private static final String JSON_SETUP_ERROR_MESSAGES_FORBIDDEN = "forbidden";
	private static final String JSON_SETUP_ERROR_MESSAGES_PARAMS_NUMBER = "paramsNummber";
	private static final String JSON_SETUP_ERROR_MESSAGES_INCORRECT_NUMBER = "incorrectNumber";
	private static final String JSON_SETUP_ERROR_SQL = "sqlError";
	private static final String JSON_SETUP_ERROR_NO_COMMAND = "noCommandFound";


	public SetUpCommand() {
		//Lecture du JSON
		JSONObject params = StaticVars.getJsonMessages();

		ERROR_MESSAGE = params.getString(JSON_ERROR_MESSAGE);

		JSONObject setupParams = params.getJSONObject(JSON_SETUP);

		HELP = setupParams.getString(JSON_SETUP_HELP);

		JSONObject commands = setupParams.getJSONObject(JSON_SETUP_COMMANDS);		
		COMMAND = commands.getString(JSON_SETUP_COMMANDS_BASE);
		COMMAND_GUILD_NUMBER = commands.getString(JSON_SETUP_COMMANDS_GUILD_NUMBER);
		COMMAND_ALLY_CODE = commands.getString(JSON_SETUP_COMMANDS_ALLY_CODE);

		JSONObject messages = setupParams.getJSONObject(JSON_SETUP_MESSAGES);
		CONFIRM_UPDATE_ALLY_CODE = messages.getString(JSON_SETUP_MESSAGES_CONFIRM_MESSAGE_ALLY_CODE);
		SETUP_ALLY_CODE_OK = messages.getString(JSON_SETUP_MESSAGES_ALLY_CODE_SETUP_OK);

		JSONObject errorMessages = setupParams.getJSONObject(JSON_SETUP_ERROR_MESSAGES);
		FORBIDDEN = errorMessages.getString(JSON_SETUP_ERROR_MESSAGES_FORBIDDEN);
		PARAMS_NUMBER = errorMessages.getString(JSON_SETUP_ERROR_MESSAGES_PARAMS_NUMBER);
		NUMBER_PROBLEM = errorMessages.getString(JSON_SETUP_ERROR_MESSAGES_INCORRECT_NUMBER);
		SQL_ERROR = errorMessages.getString(JSON_SETUP_ERROR_SQL);
		NO_COMMAND_FOUND = errorMessages.getString(JSON_SETUP_ERROR_NO_COMMAND);
	}

	@Override
	public String getCommand() {
		return COMMAND;
	}


	@Override
	public CommandAnswer answer(List<String> params, Message receivedMessage, boolean isAdmin) {

		if(params.size() < 2) {
			return new CommandAnswer(PARAMS_NUMBER,null);
		}

		if(COMMAND_ALLY_CODE.equalsIgnoreCase(params.get(0))) {
			Integer allyCode = Integer.parseInt(params.get(1).replaceAll("-",""));
			
			return setupAllyCode(receivedMessage, allyCode);
		}
		
		if(COMMAND_GUILD_NUMBER.equalsIgnoreCase(params.get(0))) {
			return new CommandAnswer("This function is not used anymore. Guild functions now use directly your ally code.",null);
		}
		
		//Actions beyond this point are admin actions
		if(!isAdmin) {
			return new CommandAnswer(FORBIDDEN, null);
		}
	
		return new CommandAnswer(error(NO_COMMAND_FOUND),null);

	}


	/**
	 * Checks if an ally-code setup is already present for this Discord User.
	 * 
	 * Then, if not present : Inserts it in databse.
	 * If present, asks the user for confirmation before inserting.
	 * @param receivedMessage
	 * @param allyCode
	 * @return
	 */
	private CommandAnswer setupAllyCode(Message receivedMessage,Integer allyCode) {

		User user = receivedMessage.getUserAuthor().get();

		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {
			conn = StaticVars.getJdbcConnection();

			stmt = conn.prepareStatement(SELECT_PLAYER);

			stmt.setLong(1,user.getId());

			rs = stmt.executeQuery();

			boolean exists = rs.next();
			
			
			Emoji emojiX = EmojiManager.getForAlias("x");
			Emoji emojiV = EmojiManager.getForAlias("white_check_mark");
			
			PlayerAllyCodeInserter inserterListener = new PlayerAllyCodeInserter(user,allyCode,receivedMessage,emojiV,emojiX);

			if(!exists) {
				
				inserterListener.insertAllyCode();
				
				return null;
			}
			else {
			
				Integer oldAllyCode = rs.getInt("allyCode");
				
				boolean different = ! oldAllyCode.equals(allyCode);
				
				if(!different) {
					return new CommandAnswer(SETUP_ALLY_CODE_OK,null);
				}
				
				String message = String.format(CONFIRM_UPDATE_ALLY_CODE, oldAllyCode);
				
				return new CommandAnswer(message, null, emojiV,emojiX).setReactionAddListener(inserterListener);
			}
		}
		catch(SQLException e) {
			logger.error(e.getMessage());
			return new CommandAnswer(SQL_ERROR, null);
		}
		finally {

			try {
				if(rs != null) {
					rs.close();
				}
				if(stmt != null) {
					stmt.close();
				}
				if(conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
				logger.error(e.getMessage());
			}

		}
		
	}
	

	private String error(String message) {
		return ERROR_MESSAGE +"**"+ message + "**\r\n\r\n"+ HELP;
	}
}
