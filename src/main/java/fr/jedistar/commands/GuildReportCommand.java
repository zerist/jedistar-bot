package fr.jedistar.commands;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.CompletableFuture;

import javax.sql.rowset.serial.SerialBlob;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.SpreadsheetVersion;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataValidation;
import org.apache.poi.ss.usermodel.DataValidationConstraint;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.AreaReference;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFDataValidationHelper;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFTable;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.javacord.api.entity.message.Message;
import org.json.JSONException;
import org.json.JSONObject;

import fr.jedistar.JediStarBotCommand;
import fr.jedistar.StaticVars;
import fr.jedistar.exception.AllyCodeNotFoundException;
import fr.jedistar.formats.CommandAnswer;
import fr.jedistar.livedata.Character;
import fr.jedistar.livedata.CharacterData;
import fr.jedistar.livedata.GuildData;
import fr.jedistar.utils.DbUtils;
import help.swgoh.api.exception.SwgohAPIDuplicateRequestException;
import help.swgoh.api.exception.SwgohAPIException;
import help.swgoh.api.exception.SwgohAPIRateLimitException;
import help.swgoh.api.exception.SwgohAPITimeoutException;

public class GuildReportCommand implements JediStarBotCommand{

	private final Logger logger = LogManager.getLogger(this.getClass());

	private final String COMMAND;
	private final String COMMAND_ALL;

	private final String ERROR_SQL;


	private final String MESSAGE_LOADING;
	private final String ERROR_SWGOH_API;
	private final String ERROR_SWGOH_API_RATE_LIMIT;
	private final String ERROR_SWGOH_API_PARTIAL_REFRESH;
	private final String ERROR_SWGOH_API_TIME_OUT;
	private final String ERROR_SWGOH_API_DUPLICATE_QUERY;

	private final String EXCEL_HEADER_SELECT_CHAR;
	private final String EXCEL_UNIT_TYPE_SHIP;
	private final String EXCEL_UNIT_TYPE_TOON;
	private final String EXCEL_FILENAME;

	private final static String SQL_FIND_HISTORY = "SELECT expiration,fileContent FROM guildReportHistory WHERE guildID=?;";
	private final static String SQL_FIND_GUILD_UNITS = "SELECT player,power,rarity,level,charID,expiration,gearLevel,zetas,combatType FROM guildUnits WHERE guildID=? ORDER BY player,charID;";
	private final static String SQL_INSERT_FILE_HISTORY = "REPLACE INTO guildReportHistory(guildID,fileContent,expiration) VALUES (?,?,?);";

	//Nom des champs JSON
	private final static String JSON_GUILD_REPORT = "guildReportCommandParameters";

	private final static String JSON_GUILD_REPORT_COMMANDS = "commands";
	private final static String JSON_GUILD_REPORT_COMMAND_MAIN = "main";
	private final static String JSON_GUILD_REPORT_COMMAND_ALL = "all";

	private final static String JSON_GUILD_REPORT_ERRORS = "errorMessages";
	private final static String JSON_GUILD_REPORT_ERROR_SQL = "sqlError";


	private final static String JSON_GUILD_REPORT_EXCEL = "excelValues";
	private final static String JSON_GUILD_REPORT_EXCEL_HEADERS = "headers";
	private final static String JSON_GUILD_REPORT_EXCEL_HEADERS_SELECT_CHAR = "selectCharacter";
	private final static String JSON_GUILD_REPORT_EXCEL_UNIT_TYPES = "unitTypes";
	private final static String JSON_GUILD_REPORT_EXCEL_UNIT_TYPES_SHIP = "ship";
	private final static String JSON_GUILD_REPORT_EXCEL_UNIT_TYPES_TOON = "toon";
	private final static String JSON_GUILD_REPORT_EXCEL_FILENAME = "filename";

	private static final String JSON_SWGOH_API = "swgohApi";
	private static final String JSON_SWGOH_API_LOADING = "loadingFromSwgohApi";
	private static final String JSON_SWGOH_API_ERROR = "swgohApiError";
	private static final String JSON_SWGOH_API_RATE_LIMIT = "rateLimit";
	private static final String JSON_SWGOH_API_PARTIAL_REFRESH = "partialRefresh";
	private static final String JSON_SWGOH_API_TIME_OUT = "timeOut";
	private static final String JSON_SWGOH_API_DUPLICATE_QUERY = "duplicateQuery";
	
	public GuildReportCommand() {
		super();

		//Lire le Json
		JSONObject parameters = StaticVars.getJsonMessages();

		JSONObject guildReportParams = parameters.getJSONObject(JSON_GUILD_REPORT);

		JSONObject commandsParams = guildReportParams.getJSONObject(JSON_GUILD_REPORT_COMMANDS);
		COMMAND = commandsParams.getString(JSON_GUILD_REPORT_COMMAND_MAIN);
		COMMAND_ALL = commandsParams.getString(JSON_GUILD_REPORT_COMMAND_ALL);

		JSONObject errorMessages = guildReportParams.getJSONObject(JSON_GUILD_REPORT_ERRORS);
		ERROR_SQL = errorMessages.getString(JSON_GUILD_REPORT_ERROR_SQL);

		JSONObject excelParams = guildReportParams.getJSONObject(JSON_GUILD_REPORT_EXCEL);

		JSONObject excelHeaders = excelParams.getJSONObject(JSON_GUILD_REPORT_EXCEL_HEADERS);
		EXCEL_HEADER_SELECT_CHAR = excelHeaders.getString(JSON_GUILD_REPORT_EXCEL_HEADERS_SELECT_CHAR);

		JSONObject excelUnitTypes = excelParams.getJSONObject(JSON_GUILD_REPORT_EXCEL_UNIT_TYPES);
		EXCEL_UNIT_TYPE_SHIP = excelUnitTypes.getString(JSON_GUILD_REPORT_EXCEL_UNIT_TYPES_SHIP);
		EXCEL_UNIT_TYPE_TOON = excelUnitTypes.getString(JSON_GUILD_REPORT_EXCEL_UNIT_TYPES_TOON);

		EXCEL_FILENAME = excelParams.getString(JSON_GUILD_REPORT_EXCEL_FILENAME);
		
		JSONObject swgohApiParams = StaticVars.getJsonMessages().getJSONObject(JSON_SWGOH_API);
		MESSAGE_LOADING = swgohApiParams.getString(JSON_SWGOH_API_LOADING);
		ERROR_SWGOH_API = swgohApiParams.getString(JSON_SWGOH_API_ERROR);
		ERROR_SWGOH_API_RATE_LIMIT = swgohApiParams.getString(JSON_SWGOH_API_RATE_LIMIT);
		ERROR_SWGOH_API_PARTIAL_REFRESH = swgohApiParams.getString(JSON_SWGOH_API_PARTIAL_REFRESH);
		ERROR_SWGOH_API_TIME_OUT = swgohApiParams.getString(JSON_SWGOH_API_TIME_OUT);
		ERROR_SWGOH_API_DUPLICATE_QUERY = swgohApiParams.getString(JSON_SWGOH_API_DUPLICATE_QUERY);
	}

	@Override
	public String getCommand() {
		return COMMAND;
	}

	@Override
	public CommandAnswer answer(List<String> params, Message receivedMessage, boolean isAdmin) throws SQLException, AllyCodeNotFoundException {
		
		Integer allyCode = DbUtils.findAllyCode(receivedMessage.getUserAuthor().get());
		
		CompletableFuture<Message> loadingMessage = receivedMessage.getChannel().sendMessage(MESSAGE_LOADING);
		
		CompletableFuture<String> guildFuture = GuildData.getGuildIDFromAllyCode(allyCode);
						
		CompletableFuture<Void> executionFuture = guildFuture.thenAccept(guildID -> {
			
			
			CompletableFuture<Calendar> refreshFuture = GuildData.refreshGuildData(allyCode,guildID);
			
			CompletableFuture<Void> innerExecutionFuture = refreshFuture.thenAccept(refreshDate -> {
				loadingMessage.thenAccept(Message::delete);
				
				receivedMessage.getChannel().type();
				
				Calendar now = Calendar.getInstance();					
				now.add(Calendar.HOUR, -1);
				
				if(now.after(refreshDate)) {
					receivedMessage.getChannel().sendMessage(String.format(ERROR_SWGOH_API_PARTIAL_REFRESH, refreshDate));
				}				
				
				receivedMessage.getChannel().type();
				
				if(params.size() == 0 || COMMAND_ALL.equals(params.get(0))) {

					boolean historyFileFound = false;
					
					if(StaticVars.isUseCache()) {
						historyFileFound = findHistoryFile(guildID,receivedMessage);
					}
					
					if(!historyFileFound) {
						receivedMessage.getApi().getThreadPool().getScheduler().submit(new GuildReportGenerator(guildID,receivedMessage));
					}
				}
				
			});
			
			innerExecutionFuture.exceptionally(e -> {

				Throwable sourceEx = e.getCause();

				sourceEx.printStackTrace();
				logger.error(sourceEx.getMessage());
				
				if(sourceEx instanceof SwgohAPIRateLimitException) {
					receivedMessage.getChannel().sendMessage(ERROR_SWGOH_API_RATE_LIMIT);
				}
				else if(sourceEx instanceof SwgohAPIDuplicateRequestException) {
					receivedMessage.getChannel().sendMessage(ERROR_SWGOH_API_DUPLICATE_QUERY);
				}
				else if(sourceEx instanceof SwgohAPITimeoutException) {
					receivedMessage.getChannel().sendMessage(ERROR_SWGOH_API_TIME_OUT);
				}
				else if(sourceEx instanceof SwgohAPIException || sourceEx instanceof JSONException) {
					receivedMessage.getChannel().sendMessage(ERROR_SWGOH_API);
				}
				
				loadingMessage.thenAccept(Message::delete);
				
				return null;
			});
		});
		
		executionFuture.exceptionally(e -> {	
			Throwable sourceEx = e.getCause();

			sourceEx.printStackTrace();
			logger.error(sourceEx.getMessage());
			
			if(sourceEx instanceof SwgohAPIRateLimitException) {
				receivedMessage.getChannel().sendMessage(ERROR_SWGOH_API_RATE_LIMIT);
			}
			else if(sourceEx instanceof SwgohAPIDuplicateRequestException) {
				receivedMessage.getChannel().sendMessage(ERROR_SWGOH_API_DUPLICATE_QUERY);
			}
			else if(sourceEx instanceof SwgohAPITimeoutException) {
				receivedMessage.getChannel().sendMessage(ERROR_SWGOH_API_TIME_OUT);
			}
			else if(sourceEx instanceof SwgohAPIException || sourceEx instanceof JSONException) {
				receivedMessage.getChannel().sendMessage(ERROR_SWGOH_API);
			}
			
			loadingMessage.thenAccept(Message::delete);
			
			return null;
		});
		


	return null;
	}

	private boolean findHistoryFile(String guildID, Message receivedMessage) {

		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {
			conn = StaticVars.getJdbcConnection();

			stmt = conn.prepareStatement(SQL_FIND_HISTORY);

			stmt.setString(1, guildID);

			rs = stmt.executeQuery();

			if(rs.next() && rs.getTimestamp("expiration").after(new Date())) {
				Blob file = rs.getBlob("fileContent");

				InputStream stream = file.getBinaryStream();

				Calendar dataDate = Calendar.getInstance();
				dataDate.setTime(rs.getTimestamp("expiration"));
				dataDate.add(Calendar.HOUR_OF_DAY, -24);
				
				String fileName = String.format(EXCEL_FILENAME,dataDate);
				receivedMessage.getChannel().sendMessage(stream,fileName);
				
				return true;
			}
			return false;

		}
		catch(SQLException e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			return false;
		}
		finally {
			try {
				if(rs != null) {
					rs.close();
				}
				if(stmt != null) {
					stmt.close();
				}
				if(conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
				logger.error(e.getMessage());
			}
		}
	}

	private class GuildReportGenerator extends Thread{

		private static final String ROSTER_SHEET_NAME = "Roster";
		String guildID;
		Message receivedMessage;

		public GuildReportGenerator(String guildID, Message receivedMessage) {
			this.guildID = guildID;
			this.receivedMessage = receivedMessage;
		}

		@Override
		public void run() {

			Workbook wb;
			
			try {
				
				byte[] template = Files.readAllBytes(Paths.get("template.xlsx"));
				
				ByteArrayInputStream stream = new ByteArrayInputStream(template);
				
				wb = new XSSFWorkbook(stream);
				
			} catch (Exception e1) {
				// Should never happen !
				e1.printStackTrace();
				logger.error(e1.getMessage());
				return;
			}

			Connection conn = null;
			PreparedStatement stmt = null;
			ResultSet rs = null;

			try {
				conn = StaticVars.getJdbcConnection();

				String query = SQL_FIND_GUILD_UNITS;
				
				stmt = conn.prepareStatement(query);

				stmt.setString(1, guildID);

				rs = stmt.executeQuery();

				XSSFSheet sheet = (XSSFSheet) wb.getSheet(ROSTER_SHEET_NAME);
				
				
				int rowCur = 1;
				Timestamp expirationDate = null;
				
				Set<String> charactersNames = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
				Set<String> playersNames = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
				
				while(rs.next()) {
					
					if(expirationDate == null) {
						expirationDate = rs.getTimestamp("expiration");
					}
				
					String baseID = rs.getString("charID");
					Character unit = CharacterData.findCharacterById(baseID, StaticVars.getLang());

					if(unit == null) {
						continue;
					}

					XSSFRow row = sheet.createRow(rowCur);

					String playerName = rs.getString("player");
					int rarity = rs.getInt("rarity");
					int power = rs.getInt("power");
					int level = rs.getInt("level");
					int gearLevel = rs.getInt("gearLevel");
					int zetas = rs.getInt("zetas");
					int combatType = rs.getInt("combatType");
					
					row.createCell(0).setCellValue(playerName);

					XSSFCell cell = row.createCell(1);
					if(Character.CHARA_TYPE_SHIP == combatType) {
						cell.setCellValue(EXCEL_UNIT_TYPE_SHIP);
					}
					else if(Character.CHARA_TYPE_TOON == combatType) {
						cell.setCellValue(EXCEL_UNIT_TYPE_TOON);
					}
					
					row.createCell(2).setCellValue(unit.name);
					row.createCell(3).setCellValue(rarity);	
					row.createCell(4).setCellValue(power);
					row.createCell(5).setCellValue(level);
					row.createCell(6).setCellValue(gearLevel);
					row.createCell(7).setCellValue(zetas);				
					row.createCell(8).setCellValue(String.format("%d / L%d / G%d / P%d", rarity,level,gearLevel,power));

					charactersNames.add(unit.name);
					playersNames.add(playerName);
					
					rowCur ++;
				}

				formatSheet(wb.getSpreadsheetVersion(),(XSSFSheet)wb.getSheet(ROSTER_SHEET_NAME),rowCur);
				
				completeBuilderSheet(wb, charactersNames, playersNames);

				wb.setActiveSheet(0);

				ByteArrayOutputStream stream = new ByteArrayOutputStream();

				wb.write(stream);
				wb.close();

				Blob blob = new SerialBlob(stream.toByteArray());

				ByteArrayInputStream inputStream = new ByteArrayInputStream(stream.toByteArray());

				Calendar dataDate = Calendar.getInstance();
				dataDate.setTime(expirationDate);
				dataDate.add(Calendar.HOUR_OF_DAY, -24);

				
				String fileName = String.format(EXCEL_FILENAME,dataDate);

				receivedMessage.getChannel().sendMessage(inputStream,fileName);

				stream.close();
				inputStream.close();
				
				rs.close();
				stmt.close();
				
				stmt = conn.prepareStatement(SQL_INSERT_FILE_HISTORY);

				stmt.setString(1, guildID);
				stmt.setTimestamp(3,expirationDate);
								
				stmt.setBlob(2, blob);

				stmt.executeUpdate();
			}
			catch(SQLException e) {
				logger.error(e.getMessage());
				e.printStackTrace();
				receivedMessage.getChannel().sendMessage(ERROR_SQL);
			} catch (Exception e) {
				//Should never happen
				e.printStackTrace();
			}
			finally {
				try {
					if(rs != null) {
						rs.close();
					}
					if(stmt != null) {
						stmt.close();
					}
					if(conn != null) {
						conn.close();
					}
				} catch (SQLException e) {
					logger.error(e.getMessage());
				}
			}

		}

		private void completeBuilderSheet(Workbook wb,Set<String> characterNames,Set<String> playerNames) {
			
			XSSFSheet sheet = (XSSFSheet) wb.getSheet("Builder");
			
			XSSFDataValidationHelper helper = new XSSFDataValidationHelper(sheet);

			XSSFSheet dataSheet = (XSSFSheet) wb.createSheet("data");
			
			int rowCur = 0;
			
			for(String charaName : characterNames) {
				Row row = dataSheet.createRow(rowCur);
				
				row.createCell(0).setCellValue(charaName);
				
				rowCur++;
			}

			DataValidationConstraint constraint = helper.createFormulaListConstraint("data!$A$1:$A$" + characterNames.size());
			
			Row row = sheet.getRow(0);
			
			int[] headers = new int[] {10,15,19,23,27,31,35};
						
			for(int i : headers) {
				Cell cell = row.getCell(i);
				
				cell.setCellValue("0 - "+EXCEL_HEADER_SELECT_CHAR);
				
				CellRangeAddressList addressList = new CellRangeAddressList(0, 0, i, i);
								
				DataValidation validation = helper.createValidation(constraint, addressList);
				
				sheet.addValidationData(validation);
			}
			
			rowCur = 2;
			for(String playerName : playerNames) {
				row = sheet.getRow(rowCur);
				
				Cell cell = row.getCell(0);
				cell.setCellValue(playerName);
				
				rowCur ++;
			}
		}

		
		private void formatSheet(SpreadsheetVersion version, XSSFSheet sheet, int rowCount) {
			if(sheet != null) {
				sheet.autoSizeColumn(0);
				sheet.setColumnWidth(0, sheet.getColumnWidth(0) + 500);
				sheet.autoSizeColumn(1);
				sheet.setColumnWidth(1, sheet.getColumnWidth(1) + 500);
				sheet.autoSizeColumn(2);
				sheet.setColumnWidth(2, sheet.getColumnWidth(2) + 500);
				sheet.autoSizeColumn(3);
				sheet.setColumnWidth(3, sheet.getColumnWidth(3) + 500);
				sheet.autoSizeColumn(4);
				sheet.setColumnWidth(4, sheet.getColumnWidth(4) + 500);
				sheet.autoSizeColumn(5);
				sheet.setColumnWidth(5, sheet.getColumnWidth(5) + 500);
				sheet.autoSizeColumn(6);
				sheet.setColumnWidth(6, sheet.getColumnWidth(6) + 500);
				sheet.autoSizeColumn(7);
				sheet.setColumnWidth(7, sheet.getColumnWidth(7) + 500);
				sheet.autoSizeColumn(8);
				sheet.setColumnWidth(8, sheet.getColumnWidth(8) + 500);
				

				XSSFTable table = sheet.getTables().get(0);

				AreaReference dataRange = new AreaReference(new CellReference(0, 0), new CellReference(rowCount, 8),version);    
				table.setArea(dataRange);

			}
		}

	}
}
