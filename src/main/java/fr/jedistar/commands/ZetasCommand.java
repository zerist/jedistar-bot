package fr.jedistar.commands;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.javacord.api.entity.message.Message;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import org.javacord.api.entity.user.User;

import fr.jedistar.JediStarBotCommand;
import fr.jedistar.StaticVars;
import fr.jedistar.exception.AllyCodeNotFoundException;
import fr.jedistar.formats.CommandAnswer;
import fr.jedistar.utils.DbUtils;
import help.swgoh.api.SwgohAPI;
import help.swgoh.api.SwgohAPI.Language;
import help.swgoh.api.SwgohAPIFilter;
import help.swgoh.api.exception.SwgohAPIDuplicateRequestException;
import help.swgoh.api.exception.SwgohAPIRateLimitException;
import help.swgoh.api.exception.SwgohAPITimeoutException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.awt.Color;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

public class ZetasCommand implements JediStarBotCommand {

	private final Logger logger = LogManager.getLogger(this.getClass());
	
    private final String COMMAND ;

    
	private final String MESSAGE_LOADING;
	private final String ERROR_SWGOH_API;
	private final String ERROR_SWGOH_API_RATE_LIMIT;
	private final String ERROR_SWGOH_API_TIME_OUT;
	private final String ERROR_SWGOH_API_DUPLICATE_QUERY;
    protected final String ERROR_LOCAL_DB;
	
    //SQL queries
    private final String SQL_FIND_LAST_UPDATE = "SELECT lastRefresh FROM playerZetas WHERE allyCode=?;";
    private final static String SQL_INSERT_ZETAS = "REPLACE INTO playerZetas VALUES (?,?,?,?);";
	private final static String SQL_SELECT_ZETAS = "SELECT chars.name,skills.name FROM playerZetas zetas INNER JOIN liveDataCharacters chars ON (chars.lang = ? AND zetas.unitName = chars.baseId) INNER JOIN liveDataSkills skills ON (skills.lang = ? AND zetas.abilityName = skills.id) WHERE allyCode = ? ORDER BY chars.name,skills.name;";
	private final static String SQL_DELETE_OLD_ZETAS = "DELETE FROM playerZetas WHERE allyCode = ? AND lastRefresh<?;";

    //Json constants
    private final static String JSON_BASE = "zetasCommandParameters";
    private final static String JSON_COMMANDS = "commands";
    private final static String JSON_COMMANDS_BASE = "base";
    
    private final static String JSON_COMMON_ERRORS = "commonErrors";
    private final static String JSON_ERROR_LOCAL_DB = "localDbError";

	private static final String JSON_SWGOH_API = "swgohApi";
	private static final String JSON_SWGOH_API_LOADING = "loadingFromSwgohApi";
	private static final String JSON_SWGOH_API_ERROR = "swgohApiError";
	private static final String JSON_SWGOH_API_RATE_LIMIT = "rateLimit";
	private static final String JSON_SWGOH_API_TIME_OUT = "timeOut";
	private static final String JSON_SWGOH_API_DUPLICATE_QUERY = "duplicateQuery";

    public ZetasCommand() {

        JSONObject params = StaticVars.getJsonMessages().getJSONObject(JSON_BASE);

        JSONObject commands = params.getJSONObject(JSON_COMMANDS);
        COMMAND = commands.getString(JSON_COMMANDS_BASE);
        
        JSONObject swgohApiParams = StaticVars.getJsonMessages().getJSONObject(JSON_SWGOH_API);
		MESSAGE_LOADING = swgohApiParams.getString(JSON_SWGOH_API_LOADING);
		ERROR_SWGOH_API = swgohApiParams.getString(JSON_SWGOH_API_ERROR);
		ERROR_SWGOH_API_RATE_LIMIT = swgohApiParams.getString(JSON_SWGOH_API_RATE_LIMIT);
		ERROR_SWGOH_API_TIME_OUT = swgohApiParams.getString(JSON_SWGOH_API_TIME_OUT);
		ERROR_SWGOH_API_DUPLICATE_QUERY = swgohApiParams.getString(JSON_SWGOH_API_DUPLICATE_QUERY);

        JSONObject errors = StaticVars.getJsonMessages().getJSONObject(JSON_COMMON_ERRORS);
        ERROR_LOCAL_DB = errors.getString(JSON_ERROR_LOCAL_DB);
    }


    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public CommandAnswer answer(List<String> params, Message receivedMessage, boolean isAdmin)throws SQLException,AllyCodeNotFoundException {
    	
        List<User> usersList = receivedMessage.getMentionedUsers();

    	if(usersList.isEmpty() && receivedMessage.getUserAuthor().isPresent())
    		usersList = Arrays.asList(receivedMessage.getUserAuthor().get());

    	for(User user : usersList) {
    		Integer allyCode = DbUtils.findAllyCode(user);

    		if(refreshNeeded(allyCode)) {
    			
    			refreshDataThenAnswer(allyCode,user, receivedMessage);
    		}
    		else {
    			generateAnswer(allyCode,user, receivedMessage,StaticVars.getLang());
    		}
    		
    	}
    	
    	return null;

    }

    private boolean refreshNeeded(Integer allyCode) throws SQLException {
    	Connection conn = StaticVars.getJdbcConnection();  	
    	PreparedStatement stmt = null;
    	ResultSet rs = null;
    	
    	try {
    		
    		stmt = conn.prepareStatement(SQL_FIND_LAST_UPDATE);
    		
    		stmt.setInt(1, allyCode);
        	
        	rs = stmt.executeQuery();
        	
        	if(! rs.next()) {
        		return true;
        	}
        	
        	Calendar lastRefreshDate = Calendar.getInstance();
            lastRefreshDate.setTime(rs.getDate("lastRefresh"));

            Calendar refreshNeededAfter = Calendar.getInstance();
            refreshNeededAfter.setTime(lastRefreshDate.getTime());
            refreshNeededAfter.add(Calendar.HOUR, 24);

            return refreshNeededAfter.before(Calendar.getInstance());
            
    	}
    	finally {
    		if (rs != null)
                rs.close();
            if (stmt != null)
                stmt.close();
            if (conn != null)
                conn.close();
    	}
    	
    }
    
    private void refreshDataThenAnswer(Integer allyCode,User user,Message receivedMessage) {
    	
		CompletableFuture<Message> messageToDelete = receivedMessage.getChannel().sendMessage(MESSAGE_LOADING);

    	SwgohAPI api = StaticVars.getSwgohApi();
    	
    	SwgohAPIFilter filter = new SwgohAPIFilter("roster",
    			new SwgohAPIFilter("defId")
    					.and("skills",new SwgohAPIFilter("tier","isZeta","id"))
    			);
    	
    	CompletableFuture<String> future = api.getPlayer(allyCode, filter);
    	
    	future.exceptionally(e -> {
    		Throwable sourceEx = e.getCause();

			sourceEx.printStackTrace();
			logger.error(sourceEx.getMessage());
			
			if(sourceEx instanceof SwgohAPIRateLimitException) {
				receivedMessage.getChannel().sendMessage(ERROR_SWGOH_API_RATE_LIMIT);
			}
			else if(sourceEx instanceof SwgohAPIDuplicateRequestException) {
				receivedMessage.getChannel().sendMessage(ERROR_SWGOH_API_DUPLICATE_QUERY);
			}
			else if(sourceEx instanceof SwgohAPITimeoutException) {
				receivedMessage.getChannel().sendMessage(ERROR_SWGOH_API_TIME_OUT);
			}
			else {
				receivedMessage.getChannel().sendMessage(ERROR_SWGOH_API);
			}
			return null;
		})
    	.thenAccept(json -> {
    		try {
    			
	    		JSONArray data = new JSONArray(json).getJSONObject(0).getJSONArray("roster");
	    					
				insertZetasData(allyCode, data);
				
				messageToDelete.thenAccept(Message::delete);
				
				generateAnswer(allyCode,user,receivedMessage,StaticVars.getLang());
				
			} catch (SQLException e1) {
				e1.printStackTrace();
				logger.error(e1.getMessage());
				receivedMessage.getChannel().sendMessage(ERROR_LOCAL_DB);
			}
    		catch(JSONException e2) {
    			e2.printStackTrace();
    			logger.error(e2.getMessage());
				receivedMessage.getChannel().sendMessage(ERROR_SWGOH_API);
    		}
		
    	});
    }
    
    private void insertZetasData(Integer allyCode,JSONArray data) throws SQLException{
    	
    	Connection conn = StaticVars.getJdbcConnection();
    	PreparedStatement stmt = null;
    	
		Timestamp updateDateTime = new Timestamp(System.currentTimeMillis());

    	try {
    		
    		conn.setAutoCommit(false);
    		
    		for(int i = 0 ; i < data.length() ; i++){
        		
        		JSONObject unitData = data.getJSONObject(i);
        			
        			String charName = unitData.getString("defId");
        			
        			stmt = conn.prepareStatement(SQL_INSERT_ZETAS);
        			
        			JSONArray charSkills= unitData.getJSONArray("skills");

        			for(int j= 0; j< charSkills.length() ; j++) {
        				
        				JSONObject skill = charSkills.getJSONObject(j);
        				if(skill.getBoolean("isZeta") && 8 == skill.getInt("tier")) {
	        				stmt.setInt(1,allyCode);
	        				stmt.setString(2,charName);
	        				stmt.setString(3,skill.getString("id"));
	        				stmt.setTimestamp(4,updateDateTime);
	
	        				stmt.addBatch();
        				}
        			}
        				
        			stmt.executeBatch();
        			conn.commit();

        			stmt.close();

        			stmt = conn.prepareStatement(SQL_DELETE_OLD_ZETAS);

        			stmt.setInt(1, allyCode);
        			stmt.setTimestamp(2, updateDateTime);

        			stmt.executeUpdate();
        			
        			conn.commit();
        			
        			stmt.close();
        		
        	}
    	}
    	finally {
            if (stmt != null)
                stmt.close();
            if (conn != null)
                conn.close();
    	}
    }
    
	private void generateAnswer(Integer allyCode, User user, Message receivedMessage, Language lang) throws SQLException{

		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		try{
			conn = StaticVars.getJdbcConnection();

			stmt = conn.prepareStatement(SQL_SELECT_ZETAS);

			stmt.setString(1,lang.getSwgohCode());
			stmt.setString(2, lang.getSwgohCode());
			stmt.setInt(3,allyCode);

			rs = stmt.executeQuery();

			EmbedBuilder embed = new EmbedBuilder();

			List<EmbedBuilder> embedsList = new ArrayList<EmbedBuilder>();

			int count = 0;
			int currentCount = 0;

			String currentCharName = "";
			String currentContent = null;
			while(rs.next()){

				count++;
				String charName = rs.getString("chars.name");

				if(currentContent != null && !StringUtils.equals(currentCharName,charName)){

					if(!StringUtils.isBlank(currentCharName)){
						currentContent += "-";
						currentCharName += " ("+currentCount+")";
						embed.addField(currentCharName,currentContent,true);
					}

					if((count % 25) == 0) {
						embedsList.add(embed);

						embed = new EmbedBuilder();

					}
					currentCharName = charName;
					currentContent = "";
					currentCount = 0;
				}

				//Init the first one
				if(currentContent == null) {
					currentContent = "";
					currentCharName = charName;
				}

				currentContent += rs.getString("skills.name")+"\r\n";
				currentCount ++;
			}

			//Insert last into embed

			currentContent += "-";
			currentCharName += " ("+currentCount+")";
			embed.addField(currentCharName,currentContent,true);

			embedsList.add(embed);

			for(EmbedBuilder curEmbed : embedsList) {
				curEmbed.setTitle("Zetas (total "+count+")");
				curEmbed.setAuthor(user);
				curEmbed.setColor(Color.CYAN);

				curEmbed.addField("-","Bot designed by [the french community Dark Side Of The Moon (DSOTM)](https://jedistar.jimdo.com)", false);

				receivedMessage.getChannel().sendMessage(curEmbed);
			}
		}
		finally{
			try{
				if(rs != null)
					rs.close();
				if(stmt != null)
					stmt.close();
				if(conn != null)
					conn.close();
			}
			catch(SQLException e){
				//Should never happen
				logger.error(e.getMessage());
				e.printStackTrace();
			}
		}
	}
}
