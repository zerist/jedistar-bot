CREATE DATABASE IF NOT EXISTS jedistar CHARACTER SET UTF8;

CREATE USER IF NOT EXISTS 'jedistar'@'localhost' IDENTIFIED BY 'JeDiStArBoT';

GRANT ALL ON jedistar.* TO 'jedistar'@'localhost';

USE jedistar;


CREATE TABLE IF NOT EXISTS characters
(
	name VARCHAR(64) PRIMARY KEY,
	baseID VARCHAR(64),
	url VARCHAR(128),
	image VARCHAR(128),
	power INTEGER,
	description VARCHAR(512),
	combatType INTEGER,
	expiration TIMESTAMP
);

CREATE TABLE IF NOT EXISTS ships
(
	name VARCHAR(64) PRIMARY KEY,
	baseID VARCHAR(64),
	url VARCHAR(128),
	image VARCHAR(128),
	power INTEGER,
	description VARCHAR(512),
	combatType INTEGER,
	expiration TIMESTAMP
);

CREATE TABLE IF NOT EXISTS guildUnits
(
	guildID VARCHAR(32) NOT NULL,
	player VARCHAR(32) NOT NULL,
	charID VARCHAR(64) NOT NULL,
	rarity INTEGER,
	combatType INTEGER,
	power INTEGER,
	level INTEGER,
	gearLevel INTEGER,
	zetas INTEGER,
	expiration TIMESTAMP,
	PRIMARY KEY (guildID,player,charID)
);

CREATE TABLE IF NOT EXISTS player
(
	discordUserID BIGINT NOT NULL PRIMARY KEY,
	allyCode INTEGER NOT NULL,
	guildID VARCHAR(32),
	guildIDExpiration TIMESTAMP
);

CREATE TABLE IF NOT EXISTS commandHistory
(
	command VARCHAR(32),
	ts TIMESTAMP,
	userID VARCHAR(64),
	userName VARCHAR(32) NOT NULL,
	serverID VARCHAR(128),
	serverName VARCHAR(128),
	serverRegion VARCHAR(64),
	executionTime INTEGER,
	PRIMARY KEY (command,ts,userID)
);

CREATE TABLE IF NOT EXISTS payoutConfig
(
	channelID varchar(64) PRIMARY KEY,
	defaultLimit INTEGER
);

CREATE TABLE IF NOT EXISTS payoutTime
(
	channelID varchar(64),
	userName varchar(64),
	payoutTime TIME,
	flag varchar(32),
	swgohggLink varchar(256),
	PRIMARY KEY (channelID,userName)
);

DELIMITER //

CREATE PROCEDURE copyPayouts (IN source VARCHAR(64),IN dest VARCHAR(64))
BEGIN
	DROP TABLE IF EXISTS temp;
	CREATE TEMPORARY TABLE temp AS SELECT * FROM payoutTime WHERE channelID=source;
	UPDATE temp set channelID=dest;
	REPLACE INTO payoutTime SELECT * FROM temp;
	DROP TABLE temp;
END//

DELIMITER ;

CREATE TABLE IF NOT EXISTS raid
(
	raidName varchar(16) PRIMARY KEY,
	raidAliases varchar(64)
);


CREATE TABLE IF NOT EXISTS guildReportHistory
(
	guildID VARCHAR(32) PRIMARY KEY,
	expiration TIMESTAMP NOT NULL,
	fileContent MEDIUMBLOB
);

CREATE TABLE IF NOT EXISTS playerZetas
(
	allyCode INTEGER NOT NULL,
	unitName VARCHAR(64) NOT NULL,
	abilityName VARCHAR(64) NOT NULL,
	lastRefresh TIMESTAMP NOT NULL,
	PRIMARY KEY (allyCode,unitName,abilityName,lang)
);

#LIVEDATA
CREATE TABLE IF NOT EXISTS liveDataCharacters
(
	baseId VARCHAR(64) NOT NULL,
	name VARCHAR(64) NOT NULL,
	lang VARCHAR(8) NOT NULL,
	PRIMARY KEY (baseId,lang)
);

CREATE TABLE IF NOT EXISTS liveDataSkills
(
	id VARCHAR(128) NOT NULL,
	name VARCHAR(128) NOT NULL,
	lang VARCHAR(8) NOT NULL,
	PRIMARY KEY (id,lang)
);

CREATE TABLE IF NOT EXISTS bannedIds
(
	discordId BIGINT NOT NULL PRIMARY KEY
);

#Index declarations
CREATE INDEX characters_baseID ON characters(baseID);
CREATE INDEX ships_baseID ON ships(baseID);
CREATE INDEX guildUnits_guildID ON guildUnits(guildID);
CREATE INDEX payoutTime_channelID ON payoutTime(channelID);
CREATE INDEX zetas_allyCode_Lang ON playerZetas(allyCode,lang);